import 'dart:convert';

import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:http/http.dart' as http;
import 'package:random_string/random_string.dart';

import '../constants/colors_constants.dart';
import '../logger.util.dart';

/// For connecting to the service that runs at https://spotify-analyzer-service.herokuapp.com.
/// The service is used to get an access- and refresh-token from the Spotify Web API.
class ConnectionService {
  String _logInUrl;
  String _accessToken;
  String _refreshToken;
  String _state;
  bool _didWebPageOpen = false;

  Duration _timeoutDuration = Duration(seconds: 45);

  // most of the errors are logged in function handleAuthError in TransitionScreen
  final log = getLogger();

  /// Connects to the service for login. A new state is created to later retrieve
  /// the access- and refresh-token correctly from the service.
  Future<dynamic> authenticate() async {
    String url = 'https://spotify-analyzer-service.herokuapp.com/api/login';
    _createState();
    Map<String, String> header = {'state': _state};

    this._didWebPageOpen = false;
    log.d('The authentication process starts. Trying to load login page.');

    return await http
        .get(Uri.parse(url), headers: header)
        .then((response) async {
          _logInUrl = response.body.toString();

          this._didWebPageOpen = true;
          log.d('Reached $url. Trying to login.');

          return await FlutterWebBrowser.openWebPage(
                  url: _logInUrl,
                  customTabsOptions: CustomTabsOptions(
                      colorScheme: CustomTabsColorScheme.system,
                      defaultColorSchemeParams: CustomTabsColorSchemeParams(
                        toolbarColor: SPOTIFY_GREEN,
                        secondaryToolbarColor: SPOTIFY_STANDARD_GREY,
                      ),
                      shareState: CustomTabsShareState.off,
                      instantAppsEnabled: true,
                      showTitle: false,
                      urlBarHidingEnabled: true))
              .timeout(_timeoutDuration,
                  onTimeout: () => Future.error("TIMEOUT"))
              .catchError((err) =>
                  throw Exception('Timed out while trying to open web page.'));
        })
        .timeout(_timeoutDuration, onTimeout: () => Future.error("TIMEOUT"))
        .catchError((err) =>
            throw Exception('Could not resolve host address of login page.\n\n'
                'Please check your internet connection.'));
  }

  /// Receives the access- and refresh-token from the service based on the state.
  ///
  /// Might only work directly after login because after a certain time of
  /// inactivity the service automatically shuts down and the state gets
  /// deleted.
  Future<bool> receiveAccessAndRefreshToken() async {
    Map<String, String> header = {'state': _state};
    String url = 'https://spotify-analyzer-service.herokuapp.com/api/tokens';

    log.d('Trying to receive the Spotify API tokens.');
    return await http
        .get(Uri.parse(url), headers: header)
        .then((response) {
      if (response.body.toString() != '') {
        _accessToken = jsonDecode(response.body)['access-token'];
        _refreshToken = jsonDecode(response.body)['refresh-token'];
      }
      bool success = _accessToken != null;
      log.i(success
          ? 'Authentication success. '
          'Received Spotify API tokens'
          : 'Authentication failed. '
          'Could not receive Spotify API tokens.');
      return success;
    })
        .timeout(_timeoutDuration, onTimeout: () => Future.error("TIMEOUT"))
        .catchError((err) {
      log.e('Failed receiving Spotify API tokens.');
      throw Exception('Could not receive Spotify API tokens.\n\n'
          'Please check your internet connection and try to login again.');
    });
  }

  /// Refreshes the access- and refresh-token.
  Future<bool> refreshTokens() async {
    var header = {'refresh-token': _refreshToken};
    String url = 'https://spotify-analyzer-service.herokuapp.com/api/refresh';

    log.d('Refreshing Spotify API tokens.');
    return await http
        .get(Uri.parse(url), headers: header)
        .then((response) {
      if (response.statusCode != 200) return false;
      _accessToken = jsonDecode(response.body)['access-token'];
      _refreshToken = jsonDecode(response.body)['refresh-token'];
      return true;
    })
        .timeout(_timeoutDuration, onTimeout: () => Future.error("TIMEOUT"))
        .catchError((err) {
      log.e('Failed refreshing Spotify API tokens.');
      throw Exception('Refreshing spotify api tokens failed.\n\n'
          'Please login again or restart the app.');
    });
  }

  /// Pings our api service to start it and check it's state.
  Future<http.Response> lifeCheck() {
    log.i("Executing lifecheck for spotify analyzer.");

    return http
        .get(Uri.parse("https://spotify-analyzer-service.herokuapp.com/api/lifecheck"))
        .then((response) {
      log.d('Lifecheck success: ' + (response.statusCode == 200).toString());
      return response;
    });
  }

  /// Creates a new "unique" state which is needed for authentication.
  String _createState() {
    _state = randomAlphaNumeric(40);
    return _state;
  }

  /// Returns the user's access token, which is needed for any Spotify API calls.
  String getAccessToken() {
    return _accessToken;
  }

  /// Clear all tokens, so that none of the user's data is saved.
  void clearTokens() {
    _accessToken = null;
    _refreshToken = null;
    log.d("Cleared the user's Spotify API tokens.");
  }

  bool didWebPageOpen() {
    return this._didWebPageOpen;
  }
}
