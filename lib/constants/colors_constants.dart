import 'package:flutter/material.dart';

// Spotify color codes. Copied from: https://www.color-hex.com/color-palette/53188
// and picked out of the spotify application

const Color SPOTIFY_GREEN = Color(0xFF1db954);

const Color SPOTIFY_BACKGROUND_GREY = Color(0xFF121212);
const Color SPOTIFY_STANDARD_GREY = Color(0xFF282828);
const Color SPOTIFY_NON_SELECTED_GREY = Color(0xFF7F7F7F);
const Color SPOTIFY_DESCRIPTION_GREY = Color(0xFFb3b3b3);

const Color SPOTIFY_WHITE = Color(0xFFFFFFFF);
const Color SPOTIFY_BLACK = Color(0xFF000000);

const Color SPOTIFY_SWITCH_STANDARD_GREEN = Color(0xFF1dd25d);
const Color SPOTIFY_SWITCH_DARK_GREEN = Color(0xFF164e29);
const Color SPOTIFY_SWITCH_STANDARD_GREY = Color(0xFFb9b9b9);
const Color SPOTIFY_SWITCH_DARK_GREY = Color(0xFF585858);

const Color SPOTIFY_SLIDER_GREEN = SPOTIFY_SWITCH_STANDARD_GREEN;
const Color SPOTIFY_SLIDER_GREY = Color(0xFF444444);
