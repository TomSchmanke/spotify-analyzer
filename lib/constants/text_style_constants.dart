import 'package:flutter/material.dart';

import 'colors_constants.dart';

const TextStyle WHITE_TITLE_TEXTSTYLE = TextStyle(color: SPOTIFY_WHITE, fontSize: 20, fontWeight: FontWeight.bold);
const TextStyle GREEN_TITLE_TEXTSTYLE = TextStyle(color: SPOTIFY_GREEN, fontSize: 20, fontWeight: FontWeight.bold);
const TextStyle GREY_TITLE_TEXTSTYLE = TextStyle(color: SPOTIFY_DESCRIPTION_GREY, fontSize: 20, fontWeight: FontWeight.bold);

const TextStyle WHITE_HEADLINE_TEXTSTYLE = TextStyle(color: SPOTIFY_WHITE, fontSize: 15, fontWeight: FontWeight.bold);
const TextStyle GREEN_HEADLINE_TEXTSTYLE = TextStyle(color: SPOTIFY_GREEN, fontSize: 15, fontWeight: FontWeight.bold);
const TextStyle GREY_HEADLINE_TEXTSTYLE = TextStyle(color: SPOTIFY_DESCRIPTION_GREY, fontSize: 15, fontWeight: FontWeight.bold);

const TextStyle WHITE_NORMAL_TEXTSTYLE = TextStyle(color: SPOTIFY_WHITE, fontSize: 15);
const TextStyle GREEN_NORMAL_TEXTSTYLE = TextStyle(color: SPOTIFY_GREEN, fontSize: 15);
const TextStyle GREY_NORMAL_TEXTSTYLE = TextStyle(color: SPOTIFY_DESCRIPTION_GREY, fontSize: 15);

const TextStyle WHITE_SMALL_TEXTSTYLE = TextStyle(color: SPOTIFY_WHITE, fontSize: 13);
const TextStyle GREEN_SMALL_TEXTSTYLE = TextStyle(color: SPOTIFY_GREEN, fontSize: 13);
const TextStyle GREY_SMALL_TEXTSTYLE = TextStyle(color: SPOTIFY_DESCRIPTION_GREY, fontSize: 13);
const TextStyle STANDARD_GREY_SMALL_TEXTSTYLE = TextStyle(color: SPOTIFY_STANDARD_GREY, fontSize: 13);

