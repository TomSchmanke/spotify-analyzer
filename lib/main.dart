import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

import 'logger.util.dart';
import 'views/login_screen.dart';

void main() {
  Logger.level = Level.debug;
  runApp(new SpotifyAnalyzerApp());
}

class SpotifyAnalyzerApp extends StatelessWidget {
  final log = getLogger();

  @override
  Widget build(BuildContext context) {
    log.i('The app was started');
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return new MaterialApp(
      title: 'Spotify Analyzer',
      debugShowCheckedModeBanner: false,
      home: new LogInScreen(),
    );
  }
}
