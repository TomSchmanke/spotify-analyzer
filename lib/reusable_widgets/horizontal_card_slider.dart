import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';

import '../logger.util.dart';

class HorizontalCardSlider extends StatelessWidget {
  final log = getLogger();
  final List<Widget> children;
  final double height;
  final int initialPage;

  HorizontalCardSlider({this.children = const [], this.height = 0, this.initialPage = 0});

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the HorizontalCardSlider screen is called');
    PageController pageController = PageController(viewportFraction: 0.8, initialPage: initialPage);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          // you may want to use an aspect ratio here for tablet
          height: this.height,
          child: PageView.builder(
            // store this controller in a State to save the carousel scroll position
            controller: pageController,
            itemCount: children.length,
            itemBuilder: (BuildContext context, int itemIndex) {
              if (itemIndex < children.length) {
                return _buildCarouselItem(context, children[itemIndex]);
              } else
                return null;
            },
          ),
        ),
        SmoothPageIndicator(
          controller: pageController,
          count: children.length,
          axisDirection: Axis.horizontal,
          effect: SlideEffect(
              spacing: 8.0,
              radius: 4.0,
              dotWidth: 8.0,
              dotHeight: 8.0,
              paintStyle: PaintingStyle.stroke,
              strokeWidth: 1.5,
              dotColor: SPOTIFY_SWITCH_STANDARD_GREY,
              activeDotColor: SPOTIFY_SWITCH_STANDARD_GREEN),
        )
      ],
    );
  }

  Widget _buildCarouselItem(BuildContext context, Widget child) {
    log.d('_buildCarouselItem was called from the build method from the HorizontalCardSlider screen');
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Padding(padding: EdgeInsets.symmetric(horizontal: 4.0), child: child),
    ]);
  }
}
