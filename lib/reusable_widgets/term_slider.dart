import 'package:flutter/material.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/views/screen_interfaces/term_slider_screens_interface.dart';

import '../logger.util.dart';

class TermSlider extends StatefulWidget {
  final IsTermSliderScreen object;

  const TermSlider(this.object);

  @override
  State<StatefulWidget> createState() => _TermSlider();
}

class _TermSlider extends State<TermSlider> {
  final log = getLogger();
  double _timeInDouble = 1;
  String _timeForDisplay = '6 months';
  String _timeAsString;

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the TermSlider screen is called');
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Term', style: WHITE_NORMAL_TEXTSTYLE,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        softWrap: false,),
      Text('Set the time frame of your collected data',
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          softWrap: false,
          style: GREY_SMALL_TEXTSTYLE),
      Slider(
        value: _timeInDouble,
        onChanged: (newValue) {
          log.i('The term slider was moved');
          setState(() {
            _timeInDouble = newValue;
            switch (_timeInDouble.round()) {
              case 0:
                _timeAsString = 'short_term';
                _timeForDisplay = '4 weeks';
                break;
              case 1:
                _timeAsString = 'medium_term';
                _timeForDisplay = '6 months';
                break;
              case 2:
                _timeAsString = 'long_term';
                _timeForDisplay = 'lifetime';
                break;
            }
            widget.object.setTime(_timeAsString);
          });
        },
        label: _timeForDisplay,
        activeColor: SPOTIFY_SLIDER_GREEN,
        inactiveColor: SPOTIFY_SLIDER_GREY,
        min: 0,
        max: 2,
        divisions: 2,
      ),
    ]);
  }
}
