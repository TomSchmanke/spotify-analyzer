import 'package:flutter/material.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/views/screen_interfaces/limit_slider_screens_interface.dart';

import '../logger.util.dart';

class LimitSlider extends StatefulWidget {
  final IsLimitSliderScreen object;

  const LimitSlider(this.object);

  @override
  State<StatefulWidget> createState() => _LimitSlider();
}

class _LimitSlider extends State<LimitSlider> {
  final log = getLogger();
  double _limitInDouble = 5;

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the LimitSlider screen is called');
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        'Number of Tracks',
        style: WHITE_NORMAL_TEXTSTYLE,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        softWrap: false,
        ),
      Text('Set the number of top tracks used for calculation',
          style: GREY_SMALL_TEXTSTYLE,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          softWrap: false,),
      Slider(
        value: _limitInDouble,
        onChanged: (newValue) {
          setState(() {
            _limitInDouble = newValue;
            widget.object.setLimit(_limitInDouble.round());
          });
        },
        label: _limitInDouble.round().toString(),
        activeColor: SPOTIFY_SLIDER_GREEN,
        inactiveColor: SPOTIFY_SLIDER_GREY,
        min: 5,
        max: 50,
        divisions: 3,
      ),
    ]);
  }
}
