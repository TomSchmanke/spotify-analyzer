import 'package:flutter/material.dart';
import 'package:multi_charts/multi_charts.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';

import '../logger.util.dart';

class CustomRadarChart extends StatelessWidget {
  final log = getLogger();
  final List<String> _labels;
  final List<double> _values;
  final double maxValue;
  final double chartRadiusFactor;
  final double textScaleFactor;
  final int maxLinesForLabels;
  final double labelWidth;

  CustomRadarChart(this._labels, this._values,
      {this.maxValue = 100,
      this.chartRadiusFactor = 1,
      this.textScaleFactor = 0.05,
      this.maxLinesForLabels = 2,
      this.labelWidth = 144});

  // creates a custom RadarChart with max value 100
  @override
  Widget build(BuildContext context) {
    log.i('The build method of the CustomRadarChart screen is called');
    // fill so that at least three data entries exist (minimum for radar chart)
    while (_labels.length < 3) {
      _labels.add("");
      _values.add(0);
    }
    return RadarChart(
      labels: this._labels,
      values: this._values,
      maxValue: this.maxValue,
      fillColor: SPOTIFY_GREEN,
      strokeColor: SPOTIFY_WHITE,
      labelColor: SPOTIFY_WHITE,
      chartRadiusFactor: this.chartRadiusFactor,
      textScaleFactor: this.textScaleFactor,
      maxLinesForLabels: this.maxLinesForLabels,
      labelWidth: this.labelWidth,
      curve: Curves.easeIn,
      animationDuration: Duration(milliseconds: 800),
    );
  }
}
