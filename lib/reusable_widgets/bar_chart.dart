import 'package:flutter/material.dart';
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/model/bar_chart_content.dart';
import 'package:spotify_analyzer/views/reusable_screens/artist_detail_screen.dart';

import '../logger.util.dart';

class BarChart extends StatefulWidget {
  final List<BarChartContent> _contentList;
  final ConnectionService _connectionService;
  final double height;

  const BarChart(this._contentList, this._connectionService, this.height);

  @override
  State<StatefulWidget> createState() => _BarChart();
}

class _BarChart extends State<BarChart> {
  final log = getLogger();
  double screenWidth;

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the BarChart screen is called');
    screenWidth = MediaQuery.of(context).size.width;
    return Container(
        height: widget.height,
        child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView(
              children: getDiagramElements(widget._contentList),
            )));
  }

  /// Creates a list of widgets to show on the screen
  List<Widget> getDiagramElements(List<BarChartContent> contentList) {
    log.d('getDiagramElements was called from the build method from the BarChart screen');
    List<Widget> childrenList = new List<Widget>();
    contentList.forEach((element) {
      childrenList.add(getDiagramElement(element));
    });
    return childrenList;
  }

  /// Returns a one element of the chart, for the values given as a parameter
  Container getDiagramElement(BarChartContent content) {
    return Container(
        padding: EdgeInsets.only(top: 5, left: 10, bottom: 5),
        child: GestureDetector(
            onTap: () {
              log.i('The bar chart has been clicked. Navigating to the artist detail screen');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ArtistDetailScreen(
                          content, widget._connectionService)));
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(content.barTitle, style: WHITE_NORMAL_TEXTSTYLE),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      child: content.image,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    buildSizedBar(content.barLength),
                  ],
                ),
              ],
            )));
  }

  /// Returns a green bar with the a size depending on the screen size and the barValue
  /// and the barValue written as a number at the end of the bar
  Stack buildSizedBar(int barLength) {
    return Stack(
      alignment: AlignmentDirectional.centerEnd,
      children: [
        Tooltip(
            message: 'Worldwide popularity',
            textStyle: WHITE_SMALL_TEXTSTYLE,
            verticalOffset: -16,
            waitDuration: Duration(milliseconds: 200),
            showDuration: Duration(milliseconds: 400),
            decoration: BoxDecoration(
              color: SPOTIFY_BACKGROUND_GREY.withAlpha(192),
              borderRadius: BorderRadius.circular(10),
            ),
            child: SizedBox(
              height: 40,
              width: ((screenWidth - 150) / 100 * barLength.toDouble()),
              child: DecoratedBox(
                decoration: BoxDecoration(
                    color: SPOTIFY_GREEN,
                    borderRadius: BorderRadius.circular(10)),
              ),
            )),
        Positioned(
          right: 10,
          child: Text(
            barLength.toString(),
            style: TextStyle(color: SPOTIFY_WHITE, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
