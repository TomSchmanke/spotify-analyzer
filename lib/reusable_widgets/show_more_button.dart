import 'package:flutter/material.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/views/screen_interfaces/show_more_button_screens_interface.dart';

import '../logger.util.dart';

class ShowMoreButton extends StatefulWidget {
  final IsShowMoreButtonScreen _object;

  const ShowMoreButton(this._object);

  @override
  State<StatefulWidget> createState() => _ShowMoreButton();
}

class _ShowMoreButton extends State<ShowMoreButton> {
  final log = getLogger();

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the ShowMoreButton screen is called');
    return Center(
      child: MaterialButton(
          child: Text(
            'Show more',
            style: GREY_SMALL_TEXTSTYLE,
          ),
          color: SPOTIFY_STANDARD_GREY,
          onPressed: () {
            log.i('Load more button was pressed');
            widget._object.setLimit(widget._object.getLength());
          }),
    );
  }
}
