import 'package:flutter/material.dart';
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/model/image_list_content.dart';
import 'package:spotify_analyzer/views/reusable_screens/playlist_analyzer_detail_screen.dart';

import '../logger.util.dart';

class ImageList extends StatefulWidget {
  final List<PlaylistDetailData> _contentList;
  final double _widgetHeight;
  final ConnectionService _connectionService;

  const ImageList(
      this._contentList, this._widgetHeight, this._connectionService);

  @override
  _ImageList createState() => _ImageList();
}

class _ImageList extends State<ImageList> {
  final log = getLogger();
  ConnectionService _connectionService;
  List<PlaylistDetailData> _contentList;
  double _widgetHeight;

  @override
  void initState() {
    log.d('The init method of the ImageList screen is called');
    _connectionService = widget._connectionService;
    _contentList = widget._contentList;
    _widgetHeight = widget._widgetHeight;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the ImageList screen is called');
    if (_contentList.isNotEmpty) {
      //Shows nothing if the list is empty
      return Column(children: [
        MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: Container(
                height: _widgetHeight,
                child: ListView(
                  children: buildList(_contentList),
                )))
      ]);
    } else {
      return SizedBox(
        height: 0,
      );
    }
  }

  ///
  /// Creates a list of widgets to show on the screen
  ///
  List<Widget> buildList(List<PlaylistDetailData> contentList) {
    log.d('buildList was called from the build method from the ImageList screen');
    List<Widget> childrenList = new List<Widget>();
    contentList.forEach((element) {
      childrenList.add(buildListElement(element));
    });
    return childrenList;
  }

  ///
  /// Returns a one element of the chart, for the values given as a parameter
  ///
  Widget buildListElement(PlaylistDetailData content) {
    return Container(
        padding: EdgeInsets.only(left: 8, right: 8, bottom: 16),
        child: GestureDetector(
            onTap: () {
              log.i('The image list has been clicked. Navigating to the playlist analyzer detail screen');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PlaylistAnalyzerDetailScreen(
                          content, _connectionService)));
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 56,
                  width: 56,
                  child: content.image,
                ),
                SizedBox(
                  width: 10,
                ),
                buildTextArea(content.title, description: content.description)
              ],
            )));
  }

  Widget buildTextArea(String title, {String description}) {
    if (description != '') {
      return Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Text(
                  title,
                  style: WHITE_TITLE_TEXTSTYLE,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  softWrap: false,
                ),
              ),
              SizedBox(
                height: 2,
              ),
              Container(
                  child: Text(description,
                      style: GREY_SMALL_TEXTSTYLE,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      softWrap: false))
            ],
          ));
    } else {
      return Text(title,
          style: WHITE_TITLE_TEXTSTYLE,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          softWrap: false);
    }
  }
}
