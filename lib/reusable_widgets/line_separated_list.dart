import 'package:flutter/material.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';

import '../logger.util.dart';

class LineSeparatedList extends StatefulWidget {
  final List<String> _contentList;
  final double _widgetHeight;
  final String title;

  const LineSeparatedList(this._contentList, this._widgetHeight, {this.title});

  @override
  _LineSeparatedList createState() => _LineSeparatedList();
}

class _LineSeparatedList extends State<LineSeparatedList> {
  final log = getLogger();
  List<String> _contentList;
  double _widgetHeight;

  @override
  void initState() {
    log.d('The init method of the LineSeparatedList screen is called');
    _contentList = widget._contentList;
    _widgetHeight = widget._widgetHeight;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the LineSeparatedList screen is called');
    if (_contentList.isNotEmpty) {
      //Shows nothing if the list is empty
      return Column(children: [
        Container(
          alignment: Alignment(-1, 0),
          child: Row(children: [
            SizedBox(width: 10),
            Expanded(
                child: Text(widget.title,
                    style: GREEN_HEADLINE_TEXTSTYLE,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    softWrap: false)),
          ]),
        ),
        MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: Container(
                height: _widgetHeight,
                child: ListView(
                  children: buildList(widget._contentList),
                )))
      ]);
    }
    return SizedBox(
      height: 0,
    );
  }

  ///
  /// Builds the list
  ///
  List<Widget> buildList(List<String> contentList) {
    log.d('buildList was called from the build method from the LineSeparatedList screen');
    List<Widget> childrenList = new List<Widget>();
    childrenList.add(buildSeparationLine());
    contentList.forEach((element) {
      childrenList.add(buildListEntry(element));
      childrenList.add(buildSeparationLine());
    });
    return childrenList;
  }

  ///
  /// Builds one row of the list
  ///
  Widget buildListEntry(String content) {
    return Container(
        alignment: Alignment(-1, 0),
        child: Row(children: [
          SizedBox(
            width: 20,
          ),
          Expanded(
              child: Text(
            content,
            style: WHITE_SMALL_TEXTSTYLE,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            softWrap: false,
          ))
        ]));
  }

  ///
  /// Builds a divider line for the list
  ///
  Widget buildSeparationLine() {
    return Divider(
      color: SPOTIFY_NON_SELECTED_GREY,
    );
  }
}
