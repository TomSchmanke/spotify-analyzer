import 'package:flutter/material.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';

import '../logger.util.dart';

SnackBar getFeatureErrorSnackBar() {
  final log = getLogger();
  log.i('The method from the ErrorSnackBar is called');
  return SnackBar(
    backgroundColor: SPOTIFY_BLACK,
    content: Text(
      "We've got problems loading your data. Please check your internet connection and try again later.",
      style: WHITE_NORMAL_TEXTSTYLE,
    ),
    duration: Duration(days: 100),
  );
}
