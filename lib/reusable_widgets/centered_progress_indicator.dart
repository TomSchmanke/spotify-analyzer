import 'package:flutter/material.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';

import '../logger.util.dart';

class CenteredProgressIndicator extends StatelessWidget {
  final log = getLogger();
  final Color mainColor;

  CenteredProgressIndicator({
    this.mainColor = SPOTIFY_GREEN,
  });

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the CenteredProgressIndicator screen is called');
    return Center(
        child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(mainColor),
    ));
  }
}
