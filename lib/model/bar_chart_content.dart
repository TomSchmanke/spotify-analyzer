import 'package:flutter/material.dart';

class BarChartContent {
  final Image image;
  final String barTitle;
  final int barLength;
  final String detailTitle;
  final String detailID;
  final String href;
  final List<String> genres;
  final int follower;

  const BarChartContent(this.image, this.barTitle, this.barLength,
      this.detailTitle, this.detailID, this.href, this.genres, this.follower);
}
