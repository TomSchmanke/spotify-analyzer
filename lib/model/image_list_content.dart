import 'package:flutter/material.dart';

class PlaylistDetailData {
  final String title;
  final String description;
  final Image image;
  final String detailHref;
  final String id;

  const PlaylistDetailData(
      this.title, this.description, this.image, this.detailHref, this.id);
}
