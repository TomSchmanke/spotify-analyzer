import 'basic_models.dart';

class PlaylistContentModel {
  final String href;
  final List<PlaylistSong> items;
  final int limit;
  final String next;
  final int offset;
  final String previous;
  final int total;

  PlaylistContentModel(
      {this.href,
      this.items,
      this.limit,
      this.next,
      this.offset,
      this.previous,
      this.total});

  factory PlaylistContentModel.fromJson(Map<String, dynamic> json) {
    var items = json['items'];
    if (items != null) {
      items = (items as List)
          .map((playlistSongJson) => PlaylistSong.fromJson(playlistSongJson))
          .toList();
    }

    return PlaylistContentModel(
      href: json['href'],
      items: items,
      limit: json['limit'],
      next: json['next'],
      offset: json['offset'],
      previous: json['previous'],
      total: json['total'],
    );
  }
}

class PlaylistSong {
  final String addedAt;
  final AddedBy addedBy;
  final bool isLocal;
  final String primaryColor;
  final PlaylistTrack track;
  final VideoThumbnail videoThumbnail;

  PlaylistSong(
      {this.addedAt,
      this.addedBy,
      this.isLocal,
      this.primaryColor,
      this.track,
      this.videoThumbnail});

  factory PlaylistSong.fromJson(Map<String, dynamic> json) {
    var addedBy = json['added_by'];
    if (addedBy != null) {
      addedBy = AddedBy.fromJson(addedBy);
    }

    var track = json['track'];
    if (track != null) {
      track = PlaylistTrack.fromJson(track);
    }

    var videoThumbnail = json['video_thumbnail'];
    if (videoThumbnail != null) {
      videoThumbnail = VideoThumbnail.fromJson(videoThumbnail);
    }

    return PlaylistSong(
      addedAt: json['added_at'],
      addedBy: AddedBy.fromJson(json['added_by']),
      isLocal: json['is_local'],
      primaryColor: json['primary_color'],
      track: track,
      videoThumbnail: videoThumbnail,
    );
  }
}

class AddedBy {
  final ExternalUrls externalUrls;
  final String href;
  final String id;
  final String type;
  final String uri;

  AddedBy({
    this.externalUrls,
    this.href,
    this.id,
    this.type,
    this.uri,
  });

  factory AddedBy.fromJson(Map<String, dynamic> json) {
    var externalUrls = json['external_urls'];
    if (externalUrls != null) {
      externalUrls = ExternalUrls.fromJson(externalUrls);
    }

    return AddedBy(
        externalUrls: ExternalUrls.fromJson(json['external_urls']),
        href: json['href'],
        id: json['id'],
        type: json['type'],
        uri: json['uri']);
  }
}

class VideoThumbnail {
  final String url;

  VideoThumbnail({this.url});

  factory VideoThumbnail.fromJson(Map<String, dynamic> json) {
    return VideoThumbnail(
      url: json['url'],
    );
  }
}

class PlaylistTrack {
  Album album;
  List<ArtistSimplified> artists;
  List<String> availableMarkets;
  int discNumber;
  int durationMs;
  bool episode;
  bool explicit;
  ExternalIds externalIds;
  ExternalUrls externalUrls;
  String href;
  String id;
  bool isLocal;
  String name;
  int popularity;
  String previewUrl;
  bool track;
  int trackNumber;
  String type;
  String uri;

  PlaylistTrack(
      {this.album,
      this.artists,
      this.availableMarkets,
      this.discNumber,
      this.durationMs,
      this.episode,
      this.explicit,
      this.externalIds,
      this.externalUrls,
      this.href,
      this.id,
      this.isLocal,
      this.name,
      this.popularity,
      this.previewUrl,
      this.track,
      this.trackNumber,
      this.type,
      this.uri});

  factory PlaylistTrack.fromJson(Map<String, dynamic> json) {
    var album = json['album'];
    if (album != null) {
      album = Followers.fromJson(album);
    }

    var artists = json['artists'];
    if (artists != null) {
      artists = (artists as List)
          .map((artistJson) => ArtistSimplified.fromJson(artistJson))
          .toList();
    }

    var availableMarkets = json['availableMarkets'];
    if (availableMarkets != null) {
      availableMarkets = (availableMarkets as List)
          .map((genreJson) => genreJson.toString())
          .toList();
    }

    return PlaylistTrack(
        album: Album.fromJson(json['album']),
        artists: artists,
        availableMarkets: availableMarkets,
        discNumber: json['discNumber'],
        durationMs: json['durationMs'],
        episode: json['episode'],
        explicit: json['explicit'],
        externalIds: ExternalIds.fromJson(json['external_ids']),
        externalUrls: ExternalUrls.fromJson(json['external_urls']),
        href: json['href'],
        id: json['id'],
        isLocal: json['isLocal'],
        name: json['name'],
        popularity: json['popularity'],
        previewUrl: json['previewUrl'],
        track: json['track'],
        trackNumber: json['trackNumber'],
        type: json['type'],
        uri: json['uri']);
  }
}
