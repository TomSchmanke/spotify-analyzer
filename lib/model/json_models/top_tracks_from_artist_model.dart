import 'basic_models.dart';

class TopTracksFromArtistModel {
  final List<Track> tracks;

  TopTracksFromArtistModel({this.tracks});

  factory TopTracksFromArtistModel.fromJson(Map<String, dynamic> json) {
    var tracks = json['tracks'];
    if (tracks != null) {
      tracks = (tracks as List)
          .map((trackJson) => Track.fromJson(trackJson))
          .toList();
    }

    return TopTracksFromArtistModel(
      tracks: tracks,
    );
  }
}
