//-------------------------------Artist Simplified------------------------------
class ArtistSimplified {
  final ExternalUrls externalUrls;
  final String href;
  final String id;
  final String name;
  final String type;
  final String uri;

  ArtistSimplified(
      {this.externalUrls, this.href, this.id, this.name, this.type, this.uri});

  factory ArtistSimplified.fromJson(Map<String, dynamic> json) {
    return ArtistSimplified(
        externalUrls: ExternalUrls.fromJson(json['external_urls']),
        href: json['href'],
        id: json['id'],
        name: json['name'],
        type: json['type'],
        uri: json['uri']);
  }
}

//--------------------------------Followers-------------------------------------
class Followers {
  final String href;
  final int total;

  Followers({this.href, this.total});

  factory Followers.fromJson(Map<String, dynamic> json) {
    return Followers(href: json['href'], total: json['total']);
  }
}

//-----------------------------------Artist-------------------------------------
class Artist {
  final ExternalUrls externalUrls;
  final Followers followers;
  final List<String> genres;
  final String href;
  final String id;
  final List<Image> images;
  final String name;
  final int popularity;
  final String type;
  final String uri;

  Artist(
      {this.externalUrls,
      this.followers,
      this.genres,
      this.href,
      this.id,
      this.images,
      this.name,
      this.popularity,
      this.type,
      this.uri});

  factory Artist.fromJson(Map<String, dynamic> json) {
    var followers = json['followers'];
    if (followers != null) {
      followers = Followers.fromJson(followers);
    }

    var genres = json['genres'];
    if (genres != null) {
      genres =
          (genres as List).map((genreJson) => genreJson.toString()).toList();
    }

    var images = json['images'];
    if (images != null) {
      images = (images as List)
          .map((imageJson) => Image.fromJson(imageJson))
          .toList();
    }

    return Artist(
        externalUrls: ExternalUrls.fromJson(json['external_urls']),
        followers: followers,
        genres: genres,
        href: json['href'],
        id: json['id'],
        images: images,
        name: json['name'],
        popularity: json['popularity'],
        type: json['type'],
        uri: json['uri']);
  }
}

//-----------------------------------Context------------------------------------
class Context {
  final ExternalUrls externalUrls;
  final String href;
  final String type;
  final String uri;

  Context({this.externalUrls, this.href, this.type, this.uri});

  factory Context.fromJson(Map<String, dynamic> json) {
    return Context(
        externalUrls: ExternalUrls.fromJson(json['external_urls']),
        href: json['href'],
        type: json['type'],
        uri: json['uri']);
  }
}

//-----------------------------------Cursors------------------------------------
class Cursors {
  final String after;
  final String before;

  Cursors({this.after, this.before});

  factory Cursors.fromJson(Map<String, dynamic> json) {
    return Cursors(after: json['after'], before: json['before']);
  }
}

//-----------------------------------ExternalIds--------------------------------
class ExternalIds {
  final String isrc;

  ExternalIds({this.isrc});

  factory ExternalIds.fromJson(Map<String, dynamic> json) {
    return ExternalIds(isrc: json['isrc']);
  }
}

//-----------------------------------ExternalUrls-------------------------------
class ExternalUrls {
  final String spotify;

  ExternalUrls({this.spotify});

  factory ExternalUrls.fromJson(Map<String, dynamic> json) {
    return ExternalUrls(spotify: json['spotify']);
  }
}

//-----------------------------------Image--------------------------------------
class Image {
  final int height;
  final String url;
  final int width;

  Image({this.height, this.url, this.width});

  factory Image.fromJson(Map<String, dynamic> json) {
    return Image(
        height: json['height'], url: json['url'], width: json['width']);
  }
}

//-----------------------------------Album--------------------------------------
class Album {
  String albumType;
  List<ArtistSimplified> artists;
  List<String> availableMarkets;
  ExternalUrls externalUrls;
  String href;
  String id;
  List<Image> images;
  String name;
  String releaseDate;
  String releaseDatePrecision;
  int totalTracks;
  String type;
  String uri;

  Album(
      {this.albumType,
      this.artists,
      this.availableMarkets,
      this.externalUrls,
      this.href,
      this.id,
      this.images,
      this.name,
      this.releaseDate,
      this.releaseDatePrecision,
      this.totalTracks,
      this.type,
      this.uri});

  factory Album.fromJson(Map<String, dynamic> json) {
    var artists = json['artists'];
    if (artists != null) {
      artists = (artists as List)
          .map((artistJson) => ArtistSimplified.fromJson(artistJson))
          .toList();
    }

    var availableMarkets = json['availableMarkets'];
    if (availableMarkets != null) {
      availableMarkets = (availableMarkets as List)
          .map((genreJson) => genreJson.toString())
          .toList();
    }

    var images = json['images'];
    if (images != null) {
      images = (images as List)
          .map((imageJson) => Image.fromJson(imageJson))
          .toList();
    }

    return Album(
        albumType: json['albumType'],
        artists: artists,
        availableMarkets: availableMarkets,
        externalUrls: ExternalUrls.fromJson(json['external_urls']),
        href: json['href'],
        id: json['id'],
        images: images,
        name: json['name'],
        releaseDate: json['releaseDate'],
        releaseDatePrecision: json['releaseDatePrecision'],
        totalTracks: json['totalTracks'],
        type: json['type'],
        uri: json['uri']);
  }
}

//------------------------------------Track-------------------------------------
class Track {
  Album album;
  List<ArtistSimplified> artists;
  List<String> availableMarkets;
  int discNumber;
  int durationMs;
  bool explicit;
  ExternalIds externalIds;
  ExternalUrls externalUrls;
  String href;
  String id;
  bool isLocal;
  String name;
  int popularity;
  String previewUrl;
  int trackNumber;
  String type;
  String uri;

  Track(
      {this.album,
      this.artists,
      this.availableMarkets,
      this.discNumber,
      this.durationMs,
      this.explicit,
      this.externalIds,
      this.externalUrls,
      this.href,
      this.id,
      this.isLocal,
      this.name,
      this.popularity,
      this.previewUrl,
      this.trackNumber,
      this.type,
      this.uri});

  factory Track.fromJson(Map<String, dynamic> json) {
    var album = json['album'];
    if (album != null) {
      album = Followers.fromJson(album);
    }

    var artists = json['artists'];
    if (artists != null) {
      artists = (artists as List)
          .map((artistJson) => ArtistSimplified.fromJson(artistJson))
          .toList();
    }

    var availableMarkets = json['availableMarkets'];
    if (availableMarkets != null) {
      availableMarkets = (availableMarkets as List)
          .map((genreJson) => genreJson.toString())
          .toList();
    }

    return Track(
        album: Album.fromJson(json['album']),
        artists: artists,
        availableMarkets: availableMarkets,
        discNumber: json['discNumber'],
        durationMs: json['durationMs'],
        explicit: json['explicit'],
        externalIds: ExternalIds.fromJson(json['external_ids']),
        externalUrls: ExternalUrls.fromJson(json['external_urls']),
        href: json['href'],
        id: json['id'],
        isLocal: json['isLocal'],
        name: json['name'],
        popularity: json['popularity'],
        previewUrl: json['previewUrl'],
        trackNumber: json['trackNumber'],
        type: json['type'],
        uri: json['uri']);
  }
}

//---------------------------------Owner----------------------------------------
class Owner {
  final String displayName;
  final ExternalUrls externalUrls;
  final String href;
  final String id;
  final String type;
  final String uri;

  const Owner(
      {this.displayName,
        this.externalUrls,
        this.href,
        this.id,
        this.type,
        this.uri});

  factory Owner.fromJson(Map<String, dynamic> json) {
    var externalUrls = json['external_urls'];
    if (externalUrls != null) {
      externalUrls = ExternalUrls.fromJson(externalUrls);
    }

    return Owner(
      displayName: json['display_name'],
      externalUrls: ExternalUrls.fromJson(json['external_urls']),
      href: json['href'],
      id: json['id'],
      type: json['type'],
      uri: json['uri'],
    );
  }
}
