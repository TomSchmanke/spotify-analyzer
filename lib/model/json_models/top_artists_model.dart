import 'basic_models.dart';

class TopArtistsModel {
  final List<Artist> items;
  final String next;
  final String previous;
  final int total;
  final int limit;
  final String href;

  TopArtistsModel(
      {this.items,
      this.next,
      this.previous,
      this.total,
      this.limit,
      this.href});

  factory TopArtistsModel.fromJson(Map<String, dynamic> json) {
    var items = json['items'];
    if (items != null) {
      items = (items as List)
          .map((artistJson) => Artist.fromJson(artistJson))
          .toList();
    }

    return TopArtistsModel(
      items: items,
      next: json['next'],
      previous: json['previous'],
      total: json['total'],
      limit: json['limit'],
      href: json['href'],
    );
  }
}
