import 'package:spotify_analyzer/model/json_models/basic_models.dart';

class PlaylistFollowerModel {
  final bool collaborative;
  final String description;
  final ExternalUrls externalUrl;
  final Followers followers;
  final String href;
  final String id;
  final List<Image> images;
  final String name;
  final Owner owner;
  final bool public;
  final String snapshotId;
  final PlaylistTracks tracks;
  final String type;
  final String uri;

  PlaylistFollowerModel(
      {this.collaborative,
      this.description,
      this.externalUrl,
      this.followers,
      this.href,
      this.id,
      this.images,
      this.name,
      this.owner,
      this.public,
      this.snapshotId,
      this.tracks,
      this.type,
      this.uri});

  factory PlaylistFollowerModel.fromJson(Map<String, dynamic> json) {
    var images = json['images'];
    if (images != null) {
      images = (images as List)
          .map((imageJson) => Image.fromJson(imageJson))
          .toList();
    }

    return PlaylistFollowerModel(
        collaborative: json['collaborative'],
        description: json['description'],
        externalUrl: ExternalUrls.fromJson(json['external_urls']),
        followers: Followers.fromJson(json['followers']),
        href: json['href'],
        id: json['id'],
        images: images,
        name: json['name'],
        owner: Owner.fromJson(json['owner']),
        public: json['public'],
        snapshotId: json['snapshot_id'],
        tracks: PlaylistTracks.fromJson(json['tracks']),
        type: json['type'],
        uri: json['uri']);
  }
}

class PlaylistTracks {
  final String href;
  final List<PlaylistItems> items;
  final int limit;
  final String next;
  final int offset;
  final String previous;
  final int total;

  PlaylistTracks(
      {this.href,
      this.items,
      this.limit,
      this.next,
      this.offset,
      this.previous,
      this.total});

  factory PlaylistTracks.fromJson(Map<String, dynamic> json) {
    var items = json['items'];
    if (items != null) {
      items = (items as List)
          .map((itemsJson) => PlaylistItems.fromJson(itemsJson))
          .toList();
    }

    return PlaylistTracks(
        href: json['href'],
        items: items,
        limit: json['limit'],
        next: json['next'],
        offset: json['offset'],
        previous: json['previous'],
        total: json['total']);
  }
}

class PlaylistItems {
  final String addedAt;
  final AddedBy addedBy;
  final bool isLocal;
  final Track track;

  PlaylistItems({this.addedAt, this.addedBy, this.isLocal, this.track});

  factory PlaylistItems.fromJson(Map<String, dynamic> json) {
    return PlaylistItems(
        addedAt: json['added_at'],
        addedBy: AddedBy.fromJson(json['added_by']),
        isLocal: json['is_local'],
        track: Track.fromJson(json['track']));
  }
}

class AddedBy {
  final ExternalUrls externalUrl;
  final String href;
  final String id;
  final String type;
  final String uri;

  AddedBy({this.externalUrl, this.href, this.id, this.type, this.uri});

  factory AddedBy.fromJson(Map<String, dynamic> json) {
    return AddedBy(
        externalUrl: ExternalUrls.fromJson(json['external_urls']),
        href: json['href'],
        id: json['id'],
        type: json['type'],
        uri: json['uri']);
  }
}
