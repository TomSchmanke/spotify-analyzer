import 'basic_models.dart';

class UserPlaylistsModel {
  final String href;
  final List<Playlists> items;
  final int limit;
  final String next;
  final int offset;
  final String previous;
  final int total;

  UserPlaylistsModel(
      {this.href,
      this.items,
      this.limit,
      this.next,
      this.offset,
      this.previous,
      this.total});

  factory UserPlaylistsModel.fromJson(Map<String, dynamic> json) {
    var items = json['items'];
    if (items != null) {
      items = (items as List)
          .map((playlistJson) => Playlists.fromJson(playlistJson))
          .toList();
    }

    return UserPlaylistsModel(
      href: json['href'],
      items: items,
      limit: json['limit'],
      next: json['next'],
      offset: json['offset'],
      previous: json['previous'],
      total: json['total'],
    );
  }
}

class Playlists {
  final bool collaborative;
  final String description;
  final ExternalUrls externalUrls;
  final String href;
  final String id;
  final List<Image> images;
  final String name;
  final Owner owner;
  final String primaryColor;
  final bool public;
  final String snapshotId;
  final TrackSimplified tracks;
  final String type;
  final String uri;

  Playlists(
      {this.collaborative,
      this.description,
      this.externalUrls,
      this.href,
      this.id,
      this.images,
      this.name,
      this.owner,
      this.primaryColor,
      this.public,
      this.snapshotId,
      this.tracks,
      this.type,
      this.uri});

  factory Playlists.fromJson(Map<String, dynamic> json) {
    var externalUrls = json['external_urls'];
    if (externalUrls != null) {
      externalUrls = ExternalUrls.fromJson(externalUrls);
    }

    var images = json['images'];
    if (images != null) {
      images = (images as List)
          .map((imageJson) => Image.fromJson(imageJson))
          .toList();
    }

    var owner = json['owner'];
    if (owner != null) {
      owner = Owner.fromJson(owner);
    }

    var tracks = json['tracks'];
    if (tracks != null) {
      tracks = TrackSimplified.fromJson(tracks);
    }

    return Playlists(
      collaborative: json['collaborative'],
      description: json['description'],
      externalUrls: ExternalUrls.fromJson(json['external_urls']),
      href: json['href'],
      id: json['id'],
      images: images,
      name: json['name'],
      owner: owner,
      primaryColor: json['primary_color'],
      public: json['public'],
      snapshotId: json['snapshot_id'],
      tracks: tracks,
      type: json['type'],
      uri: json['uri'],
    );
  }
}

class TrackSimplified {
  final String href;
  final int total;

  const TrackSimplified({this.href, this.total});

  factory TrackSimplified.fromJson(Map<String, dynamic> json) {
    return TrackSimplified(
      href: json['href'],
      total: json['total'],
    );
  }
}
