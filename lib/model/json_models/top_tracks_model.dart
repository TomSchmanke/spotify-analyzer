import 'basic_models.dart';

class TopTracksModel {
  final List<Track> items;
  final String next;
  final String previous;
  final int total;
  final int limit;
  final String href;

  TopTracksModel(
      {this.items,
      this.next,
      this.previous,
      this.total,
      this.limit,
      this.href});

  factory TopTracksModel.fromJson(Map<String, dynamic> json) {
    var items = json['items'];
    if (items != null) {
      items = (items as List)
          .map((trackJson) => Track.fromJson(trackJson))
          .toList();
    }

    return TopTracksModel(
      items: items,
      next: json['next'],
      previous: json['previous'],
      total: json['total'],
      limit: json['limit'],
      href: json['href'],
    );
  }
}
