class AudioFeaturesModel {
  final double danceability;
  final double energy;
  final double loudness;
  final double speechiness;
  final double acousticness;
  final double instrumentalness;
  final double liveness;
  final double valence;
  final double tempo;
  final String analysisUrl;

  AudioFeaturesModel(
      {this.danceability,
      this.energy,
      this.loudness,
      this.speechiness,
      this.acousticness,
      this.instrumentalness,
      this.liveness,
      this.valence,
      this.tempo,
      this.analysisUrl});

  factory AudioFeaturesModel.fromJson(Map<String, dynamic> json) {
    return AudioFeaturesModel(
        danceability: json['danceability'],
        energy: json['energy'],
        loudness: json['loudness'],
        speechiness: json['speechiness'],
        acousticness: json['acousticness'],
        instrumentalness: json['instrumentalness'].toDouble(),
        liveness: json['liveness'],
        valence: json['valence'],
        tempo: json['tempo'],
        analysisUrl: json['analysis_url']);
  }
}
