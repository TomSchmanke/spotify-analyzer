import 'dart:math';

import 'package:flutter/material.dart' as DartImage;
import 'package:flutter/material.dart';
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/data_handling/artist_data_loader.dart';
import 'package:spotify_analyzer/model/bar_chart_content.dart';
import 'package:spotify_analyzer/model/json_models/basic_models.dart';
import 'package:spotify_analyzer/model/json_models/basic_models.dart'
    as SpotifyImage;
import 'package:spotify_analyzer/model/json_models/top_artists_model.dart';
import 'package:spotify_analyzer/reusable_widgets/bar_chart.dart';
import 'package:spotify_analyzer/reusable_widgets/centered_progress_indicator.dart';
import 'package:spotify_analyzer/reusable_widgets/feature_error_snack_bar.dart';
import 'package:spotify_analyzer/reusable_widgets/show_more_button.dart';
import 'package:spotify_analyzer/reusable_widgets/term_slider.dart';
import 'package:spotify_analyzer/views/reusable_screens/master_screen.dart';
import 'package:spotify_analyzer/views/screen_interfaces/show_more_button_screens_interface.dart';
import 'package:spotify_analyzer/views/screen_interfaces/term_slider_screens_interface.dart';

import '../../logger.util.dart';

class TopArtistsScreen extends StatefulWidget {
  final ConnectionService connectionService;

  const TopArtistsScreen(this.connectionService);

  @override
  _TopArtistsScreen createState() => _TopArtistsScreen();
}

class _TopArtistsScreen extends State<TopArtistsScreen>
    implements IsShowMoreButtonScreen, IsTermSliderScreen {
  final log = getLogger();

  ConnectionService _connectionService;

  /// time_range must be either short_term, medium_term (default) or long_term
  String _timeAsString = 'medium_term';

  /// maximum artist count must be positive integer (maximum is 50)
  int _limit = 5;
  List<Artist> _artists;

  ArtistDataLoader _artistDataLoader;
  double _screenHeight;
  double _extraListViewHeight;

  @override
  void setTime(String time) {
    this._timeAsString = time;
    setState(() {});
  }

  @override
  void initState() {
    log.d('The init method of the top artists screen is called');
    super.initState();
    this._connectionService = widget.connectionService;
    _artistDataLoader = new ArtistDataLoader(_connectionService);
  }

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the top artists screen is called');

    _screenHeight = MediaQuery.of(context).size.height;
    return MasterScreen(
      title: 'Top Artists',
      backgroundColor: SPOTIFY_BACKGROUND_GREY,
      child: FutureBuilder<TopArtistsModel>(
        future: _artistDataLoader.getTopArtists(
            timeRange: _timeAsString, limit: 50),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            this._artists =
                snapshot.data.items != null ? snapshot.data.items : [];
            this._extraListViewHeight =
                _shouldShowMoreButtonBeVisible() ? 0 : 48;
            return Container(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      BarChart(
                          _buildBarChartContent(this
                              ._artists
                              .sublist(0, min(_limit, this._artists.length))),
                          _connectionService,
                          _screenHeight - 194 + _extraListViewHeight),
                      SizedBox(
                        height: 15,
                      ),
                      if (_shouldShowMoreButtonBeVisible())
                        ShowMoreButton(this),
                      TermSlider(this),
                    ]));
          } else if (snapshot.hasError) {
            log.e(
                'The future builder which loads the top artists ran into an unexpected error');
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              ScaffoldMessenger.of(context).showSnackBar(getFeatureErrorSnackBar());
            });
            return CenteredProgressIndicator();
          }
          log.d(
              'The future builder which loads the top artists is loading data');
          // By default, show a loading spinner.
          return CenteredProgressIndicator();
        },
      ),
    );
  }

  /// returns a list of BarChartContent for each Artist in [artists]
  List<BarChartContent> _buildBarChartContent(List<Artist> artists) {
    log.d(
        '_buildBarChartContent was called from the build method from the top artists screen');
    return artists
        .map((artist) => BarChartContent(
            _getFirstQuadraticImage(artist.images),
            artist.name,
            artist.popularity,
            artist.name,
            null,
            artist.href,
            artist.genres,
            artist.followers.total))
        .toList();
  }

  /// Methods go through the images provided in a list and returns the first image which has the same height and width.
  /// If no image is found the first one will be returned
  DartImage.Image _getFirstQuadraticImage(List<SpotifyImage.Image> imageList) {
    if (imageList.isEmpty) {
      return DartImage.Image.asset('lib/assets/not_available.png');
    }
    for (SpotifyImage.Image image in imageList) {
      if (image.height == image.width) {
        return DartImage.Image.network(image.url);
      }
    }
    return DartImage.Image.network(imageList.elementAt(0).url);
  }

  /// returns length of the loaded tracks
  @override
  int getLength() {
    return this._artists.length;
  }

  /// set the limit of appearing tracks in top tracks list
  @override
  void setLimit(int limit) {
    this.setState(() {
      this._limit = limit;
    });
  }

  /// returns true if the currently shown tracks (limit) are not all available track,
  bool _shouldShowMoreButtonBeVisible() {
    log.d(
        '_shouldShowMoreButtonBeVisible was called from the build method from the top artists screen');
    return this._limit < this.getLength();
  }
}
