import 'package:flutter/material.dart';
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/data_handling/audio_features_data_loader.dart';
import 'package:spotify_analyzer/data_handling/track_data_loader.dart';
import 'package:spotify_analyzer/model/json_models/audio_feature_model.dart';
import 'package:spotify_analyzer/reusable_widgets/centered_progress_indicator.dart';
import 'package:spotify_analyzer/reusable_widgets/custom_radar_chart.dart';
import 'package:spotify_analyzer/reusable_widgets/feature_error_snack_bar.dart';
import 'package:spotify_analyzer/reusable_widgets/limit_slider.dart';
import 'package:spotify_analyzer/reusable_widgets/term_slider.dart';
import 'package:spotify_analyzer/views/reusable_screens/audio_features_info.dart';
import 'package:spotify_analyzer/views/reusable_screens/master_screen.dart';
import 'package:spotify_analyzer/views/screen_interfaces/limit_slider_screens_interface.dart';
import 'package:spotify_analyzer/views/screen_interfaces/term_slider_screens_interface.dart';

import '../../logger.util.dart';

class AudioFeaturesScreen extends StatefulWidget {
  final ConnectionService _connectionService;

  const AudioFeaturesScreen(this._connectionService);

  @override
  _AudioFeaturesScreen createState() => _AudioFeaturesScreen();
}

class _AudioFeaturesScreen extends State<AudioFeaturesScreen>
    implements IsLimitSliderScreen, IsTermSliderScreen {
  final log = getLogger();

  AudioFeaturesDataLoader _audioFeaturesLoader;
  TrackDataLoader _trackDataLoader;
  ConnectionService _connectionService;

  /// time_range must be either short_term, medium_term (default) or long_term
  String _timeRange = 'medium_term';

  /// maximum limit of tracks must be positive (maximum is 50)
  int _limit = 5;

  double _screenHeight;

  final List<String> _radarChartLabels = [
    'Dance',
    'Live',
    'Speech',
    'Acustic',
    'Instrumental',
    'Valence'
  ];

  @override
  void initState() {
    log.d('The init method of the audio features screen is called');
    super.initState();
    this._connectionService = widget._connectionService;
    _audioFeaturesLoader = new AudioFeaturesDataLoader(_connectionService);
    _trackDataLoader = new TrackDataLoader(_connectionService);
  }

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the audio features screen is called');

    _screenHeight = MediaQuery.of(context).size.height;

    return MasterScreen(
        title: 'Audio Features',
        backgroundColor: SPOTIFY_BACKGROUND_GREY,
        child: Container(
          padding: EdgeInsets.only(left: 10),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _createInfoContainer(),
                Container(
                    height: _screenHeight - 276,
                    child: FutureBuilder(
                        // loads audio features to the top tracks ids
                        future: _getAudioFeatureData(),
                        builder: (context, audioFeatures) {
                          if (audioFeatures.connectionState ==
                              ConnectionState.done) {
                            if (audioFeatures.hasData) {
                              List<double> radarData =
                                  _calculateDataForRadarChart(
                                      audioFeatures.data);
                              return CustomRadarChart(
                                _radarChartLabels,
                                radarData,
                                chartRadiusFactor: 0.6,
                                textScaleFactor: 0.045,
                              );
                            } else if (audioFeatures.hasError) {
                              log.e(
                                  'The future builder which loads the audio features ran into an unexpected error');
                              WidgetsBinding.instance
                                  .addPostFrameCallback((timeStamp) {
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(getFeatureErrorSnackBar());
                              });
                              return CenteredProgressIndicator();
                            }
                          }
                          log.d(
                              'The future builder which loads the audio features is loading data');
                          return CenteredProgressIndicator();
                        })),
                SizedBox(
                  height: 16,
                ),
                LimitSlider(this),
                TermSlider(this),
              ]),
        ));
  }

  /// Creates an info icon inside a container, that on-click leads to an InfoScreen
  Container _createInfoContainer() {
    log.d(
        '_createInfoContainer was called from the build method from the audio features screen');
    return Container(
      alignment: Alignment.centerRight,
      child: IconButton(
        icon: Icon(Icons.info_outline),
        iconSize: 30,
        color: SPOTIFY_WHITE,
        tooltip: 'Get a description about the audio features',
        onPressed: () {
          log.i('information button in the audio features was pressed');
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AudioFeaturesInfoScreen()));
        },
      ),
    );
  }

  /// Returns List of all AudioFeatures in TopTracks
  /// as List<AudioFeaturesModel>-Future (uses API Calls)
  Future<List<AudioFeaturesModel>> _getAudioFeatureData() {
    return _trackDataLoader
        .getTopTracksIds(timeRange: _timeRange, limit: _limit)
        .then((trackIds) {
      return _audioFeaturesLoader.getAudioFeatures(trackIds);
    });
  }

  /// Calculates radar chart data as List with values
  List<double> _calculateDataForRadarChart(
      List<AudioFeaturesModel> audioFeatures) {
    log.d(
        '_calculateDataForRadarChart was called from the build method from the audio features screen');
    double danceability = 0;
    double liveness = 0;
    double speechiness = 0;
    double acousticness = 0;
    double instrumentalness = 0;
    double valence = 0;

    // the value of an audio features is between 0.0 and 1.0 to use the values
    // in the radar chart they need to be converted to integers between 0 and 100
    audioFeatures.forEach((audioFeature) {
      danceability += (audioFeature.danceability * 100);
      liveness += (audioFeature.liveness * 100);
      speechiness += (audioFeature.speechiness * 100);
      acousticness += (audioFeature.acousticness * 100);
      instrumentalness += (audioFeature.instrumentalness * 100);
      valence += (audioFeature.valence * 100);
    });

    return _calculateAverageValues(
        AudioFeaturesModel(
            danceability: danceability,
            liveness: liveness,
            speechiness: speechiness,
            acousticness: acousticness,
            instrumentalness: instrumentalness,
            valence: valence),
        audioFeatures.length);
  }

  /// To get the average values each audio feature needs to be divided by the
  /// amount of tracks that are analyzed.
  List<double> _calculateAverageValues(AudioFeaturesModel total, int length) {
    return [
      total.danceability / length,
      total.liveness / length,
      total.speechiness / length,
      total.acousticness / length,
      total.instrumentalness / length,
      total.valence / length,
    ];
  }

  @override
  void setTime(String time) {
    this._timeRange = time;
    setState(() {});
  }

  @override
  void setLimit(int limit) {
    this._limit = limit;
    setState(() {});
  }
}
