import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as DartImage;
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/data_handling/playlist_data_loader.dart';
import 'package:spotify_analyzer/model/image_list_content.dart';
import 'package:spotify_analyzer/model/json_models/basic_models.dart'
    as SpotifyImage;
import 'package:spotify_analyzer/model/json_models/playlist_model.dart';
import 'package:spotify_analyzer/reusable_widgets/centered_progress_indicator.dart';
import 'package:spotify_analyzer/reusable_widgets/feature_error_snack_bar.dart';
import 'package:spotify_analyzer/reusable_widgets/image_list.dart';
import 'package:spotify_analyzer/views/reusable_screens/master_screen.dart';

import '../../logger.util.dart';

class PlaylistAnalyzerScreen extends StatefulWidget {
  final ConnectionService connectionService;

  const PlaylistAnalyzerScreen(this.connectionService);

  @override
  _PlaylistAnalyzerScreen createState() => _PlaylistAnalyzerScreen();
}

class _PlaylistAnalyzerScreen extends State<PlaylistAnalyzerScreen> {
  final log = getLogger();

  ConnectionService _connectionService;
  PlaylistDataLoader _playlistDataLoader;
  double screenHeight;

  @override
  void initState() {
    log.d('The init method of the playlist analyzer screen is called');
    super.initState();
    _connectionService = widget.connectionService;
    _playlistDataLoader = new PlaylistDataLoader(_connectionService);
  }

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the playlist analyzer screen is called');

    screenHeight = MediaQuery.of(context).size.height;
    return MasterScreen(
      title: "Playlist Analyzer",
      backgroundColor: SPOTIFY_BACKGROUND_GREY,
      child: FutureBuilder<UserPlaylistsModel>(
        future: _playlistDataLoader.getPersonalPlaylists(limit: 50, offset: 0),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: [
                ImageList(_buildPlaylistDetailDataList(snapshot.data),
                    screenHeight - 50, _connectionService)
              ],
            );
          } else if (snapshot.hasError) {
            log.e(
                'The future builder which loads the personal playlists ran into an unexpected error');
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              ScaffoldMessenger.of(context).showSnackBar(getFeatureErrorSnackBar());
            });
            return CenteredProgressIndicator();
          }
          log.d(
              'The future builder which loads the personal playlists is loading data');
          // By default, show a loading spinner.
          return CenteredProgressIndicator();
        },
      ),
    );
  }

  List<PlaylistDetailData> _buildPlaylistDetailDataList(
      UserPlaylistsModel userPlaylistsModel) {
    log.d(
        '_buildPlaylistDetailDataList was called from the build method from the playlist analyzer screen');
    List<PlaylistDetailData> imageListContents = [];

    userPlaylistsModel.items.forEach((element) {
      imageListContents.add(PlaylistDetailData(
          element.name,
          element.tracks.total.toString() + " Songs",
          getFirstQuadraticImage(element.images),
          element.tracks.href,
          element.id));
    });

    return imageListContents;
  }

  ///
  /// Methods go through the images provided in a list and returns the first image which has the same height and width.
  /// If no image is found the first one will be returned
  ///
  DartImage.Image getFirstQuadraticImage(List<SpotifyImage.Image> imageList) {
    if (imageList.isEmpty) {
      return DartImage.Image.asset('lib/assets/not_available.png');
    }
    for (SpotifyImage.Image image in imageList) {
      if (image.height == image.width) {
        return DartImage.Image.network(image.url);
      }
    }
    return DartImage.Image.network(imageList.elementAt(0).url);
  }
}
