import 'dart:math';

import 'package:flutter/material.dart';
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/data_handling/artist_data_loader.dart';
import 'package:spotify_analyzer/data_handling/track_data_loader.dart';
import 'package:spotify_analyzer/model/json_models/basic_models.dart';
import 'package:spotify_analyzer/model/json_models/top_tracks_model.dart';
import 'package:spotify_analyzer/reusable_widgets/centered_progress_indicator.dart';
import 'package:spotify_analyzer/reusable_widgets/custom_radar_chart.dart';
import 'package:spotify_analyzer/reusable_widgets/feature_error_snack_bar.dart';
import 'package:spotify_analyzer/reusable_widgets/term_slider.dart';
import 'package:spotify_analyzer/views/reusable_screens/master_screen.dart';
import 'package:spotify_analyzer/views/screen_interfaces/term_slider_screens_interface.dart';

import '../../logger.util.dart';

class TopGenresScreen extends StatefulWidget {
  final ConnectionService connectionService;

  const TopGenresScreen(this.connectionService);

  @override
  _TopGenresScreen createState() => _TopGenresScreen();
}

class _TopGenresScreen extends State<TopGenresScreen>
    implements IsTermSliderScreen {
  final log = getLogger();

  ConnectionService _connectionService;

  /// time_range must be either short_term, medium_term (default) or long_term
  String timeRange = "medium_term";
  final int limit = 50;

  ArtistDataLoader _artistDataLoader;
  TrackDataLoader _trackDataLoader;

  double screenHeight;

  /// max number of data entries for radar chart
  final int maxRadarChartEntries = 12;
  final double minPercentageToDisplay = 0.1;
  bool _groupingGenres = false;

  @override
  void initState() {
    log.d('The init method of the top genres screen is called');
    super.initState();
    this._connectionService = widget.connectionService;
    _artistDataLoader = new ArtistDataLoader(_connectionService);
    _trackDataLoader = new TrackDataLoader(_connectionService);
  }

  @override
  void setTime(String time) {
    this.timeRange = time;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the top genres screen is called');

    screenHeight = MediaQuery.of(context).size.height;
    return MasterScreen(
        title: "Top Genres",
        backgroundColor: SPOTIFY_BACKGROUND_GREY,
        child: Container(
          height: screenHeight - 50,
          padding: EdgeInsets.only(left: 12, right: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: FutureBuilder(
                      // loads audio features to the top tracks ids
                      future: _getAllGenresCounted(),
                      builder: (context, topGenres) {
                        if (topGenres.connectionState == ConnectionState.done) {
                          if (topGenres.hasData) {
                            Map<String, dynamic> radarData =
                                _calcRadarChartData(topGenres.data,
                                    sortingType: SortingType.alphabetical);
                            return Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: 12, right: 12),
                                          child: CustomRadarChart(
                                              radarData['labels'],
                                              radarData['values']),
                                        )
                                      ]))
                                ]);
                          } else if (topGenres.hasError) {
                            log.e(
                                'The future builder which loads the top genres ran into an unexpected error');
                            WidgetsBinding.instance
                                .addPostFrameCallback((timeStamp) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(getFeatureErrorSnackBar());
                            });
                            return CenteredProgressIndicator();
                          }
                        }
                        log.d(
                            'The future builder which loads the top genres is loading data');
                        return CenteredProgressIndicator();
                      })),
              TermSlider(this),
              SizedBox(height: 4),
              _createGroupSwitchRow(),
            ],
          ),
        ));
  }

  /// Returns list of all top tracks' ArtistIDs as List<String> by using
  /// jsonLoader.getTopTracks(...)
  Future<List<String>> _getTopTracksArtistsIds() async {
    TopTracksModel topTracksModel =
        await _trackDataLoader.getTopTracks(timeRange: timeRange, limit: limit);
    return _getAllArtistIdsFromTracks(topTracksModel.items);
  }

  /// Returns a List<String> of all artist ids for the given [tracks]
  List<String> _getAllArtistIdsFromTracks(List<Track> tracks) {
    // 1: extract the artists, 2: flatten the 2D List, 3: extract the ids
    return tracks
        .map((track) => track.artists)
        .expand((artists) => artists)
        .map((artist) => artist.id)
        .toList();
  }

  /// Returns map of all genres and their total occurrences in TopTracks-Artists
  /// as Map<String, int>-Future (uses API Calls)
  /// with key as genre name (String) and value as counted occurrences of that genre
  Future<Map<String, int>> _getAllGenresCounted() async {
    log.d(
        '_getAllGenresCounted was called from the build method from the top genres screen');
    return _getTopTracksArtistsIds().then((artistIds) async {
      return _getGenresOfArtists(artistIds).then((genres) {
        Map<String, int> genreCount = new Map();
        genres.forEach((genre) {
          if (!genreCount.containsKey(genre)) {
            genreCount[genre] = 1;
          } else {
            genreCount[genre] += 1;
          }
        });
        return genreCount;
      });
    });
  }

  /// Returns a list of all genres for the given [artistIds] (includes duplicates)
  Future<List<String>> _getGenresOfArtists(List<String> artistIds) async {
    // split all artist ids into chunks of 50
    // so that the spotify api can handle the request below with max 50 ids:
    List<List<String>> splitArtistIdsBy50 = [];
    for (int n = 0; n < artistIds.length; n += 50) {
      splitArtistIdsBy50
          .add(artistIds.sublist(n, min(n + 50, artistIds.length)));
    }

    // collect all artists' genres
    List<String> genres = [];
    for (List<String> artistIds in splitArtistIdsBy50) {
      List<Artist> artists = await _artistDataLoader.getArtistsByIds(artistIds);
      // collect only genres, flatten them into List<String> and add to genres
      genres.addAll(artists.map((artist) => artist.genres).expand((g) => g));
    }
    return genres;
  }

  /// Calculates radar chart data as Map with entries
  /// labels: List<String>, values: List<double>
  Map<String, dynamic> _calcRadarChartData(Map<String, int> genreCount,
      {SortingType sortingType}) {
    log.d(
        '_calcRadarChartData was called from the build method from the top genres screen');

    // Optionally group by genre if switch is on true
    if (_groupingGenres) genreCount = _groupGenres(genreCount);

    // sort topGenres by values and write sorted keys to list (with max n entries)
    List<String> sortedGenreNames = genreCount.keys.toList()
      ..sort((k1, k2) => genreCount[k2].compareTo(genreCount[k1]));
    // find maximum value
    int maxValue =
        sortedGenreNames.length > 0 ? genreCount[sortedGenreNames[0]] : 1;

    /// remove all low value-entries until length of maxRadarChartEntries reached.
    /// remove all entries with values below minPercentageToDisplay * maxValue
    sortedGenreNames
      ..removeRange(
          min(maxRadarChartEntries, genreCount.length), genreCount.length)
      ..removeWhere(
          (element) => genreCount[element] < minPercentageToDisplay * maxValue);

    // if intended, sort entries differently
    switch (sortingType) {
      case SortingType.value:
        // already sorted by values (see above)
        break;
      case SortingType.alphabetical:
        sortedGenreNames.sort((g1, g2) => g1.compareTo(g2));
        break;
      case SortingType.random:
        sortedGenreNames..shuffle(new Random());
        break;
    }

    // get values for RadarChart from genreCount[genreName]
    List<double> values = sortedGenreNames
        .map((genreName) => 100 * genreCount[genreName] / maxValue)
        .toList();

    return {'labels': sortedGenreNames, 'values': values};
  }

  /// groups genres depending on (only!) single keywords.
  /// E.g.: 'hip hop' will be split into 'hip' and 'hop'
  Map<String, int> _groupGenres(Map<String, int> genreCount) {
    Map<List<String>, int> genresCount =
        genreCount.map((key, value) => MapEntry(key.split(' '), value));

    // count each keyword
    Map<String, int> keywordCount = new Map();
    genresCount.forEach((keywords, count) {
      keywords.forEach((keyword) {
        keywordCount.update(keyword, (value) => value + count,
            ifAbsent: () => count);
      });
    });
    return keywordCount;
  }

  /// creates a switch that is able to  boolean [_groupingGenres], so that genres
  /// can be grouped by keywords
  Row _createGroupSwitchRow() {
    log.d('_createGroupSwitchRow was called from the build method from the top genres screen');
    return Row(
      children: <Widget>[
        Container(
          child: Expanded(
              child: Text(
            "Group by Keywords",
            style: WHITE_NORMAL_TEXTSTYLE,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            softWrap: false,
          )),
        ),
        // your red container
        Container(
          child: Switch(
            value: _groupingGenres,
            onChanged: (bool value) {
              log.i('group genres switch was pressed');
              setState(() {
                _groupingGenres = value;
              });
            },
            activeColor: SPOTIFY_SWITCH_STANDARD_GREEN,
            activeTrackColor: SPOTIFY_SWITCH_DARK_GREEN,
            inactiveThumbColor: SPOTIFY_SWITCH_STANDARD_GREY,
            inactiveTrackColor: SPOTIFY_SWITCH_DARK_GREY,
          ),
        ),
        // your blue container
      ],
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
    );
  }
}

enum SortingType { value, alphabetical, random }
