import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as DartImage;
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/data_handling/artist_data_loader.dart';
import 'package:spotify_analyzer/data_handling/track_data_loader.dart';
import 'package:spotify_analyzer/model/bar_chart_content.dart';
import 'package:spotify_analyzer/model/json_models/basic_models.dart';
import 'package:spotify_analyzer/model/json_models/top_tracks_from_artist_model.dart';
import 'package:spotify_analyzer/reusable_widgets/centered_progress_indicator.dart';
import 'package:spotify_analyzer/reusable_widgets/feature_error_snack_bar.dart';
import 'package:spotify_analyzer/reusable_widgets/horizontal_card_slider.dart';
import 'package:spotify_analyzer/reusable_widgets/line_separated_list.dart';
import 'package:spotify_analyzer/views/reusable_screens/master_screen.dart';

import '../../logger.util.dart';

class ArtistDetailScreen extends StatefulWidget {
  final BarChartContent barCharContent;
  final ConnectionService connectionService;

  const ArtistDetailScreen(this.barCharContent, this.connectionService);

  @override
  _ArtistDetailScreen createState() => _ArtistDetailScreen();
}

class _ArtistDetailScreen extends State<ArtistDetailScreen> {
  final log = getLogger();

  double screenHeight;
  BarChartContent barChartContent;
  ConnectionService _connectionService;
  ArtistDataLoader _artistDataLoader;
  TrackDataLoader _trackDataLoader;

  // widget heights
  final double heightCardSlider = 320;

  @override
  void initState() {
    log.d('The init method of the artist details screen screen is called');
    super.initState();
    barChartContent = widget.barCharContent;
    _connectionService = widget.connectionService;
    _artistDataLoader = new ArtistDataLoader(_connectionService);
    _trackDataLoader = new TrackDataLoader(_connectionService);
  }

  @override
  Widget build(BuildContext context) {
    log.i('The build method of the artist details screen is called');
    screenHeight = MediaQuery.of(context).size.height;
    return MasterScreen(
        title: "Artist Details",
        backgroundColor: SPOTIFY_STANDARD_GREY,
        child: _buildContent());
  }

  Widget _buildContent() {
    log.d(
        '_buildContent was called from the build method from the artist details screen');
    if (barChartContent.detailID == null) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        textBaseline: TextBaseline.alphabetic,
        children: [
          _createHorizontalCardSlider(barChartContent.detailTitle,
              barChartContent.image, barChartContent.follower),
          _buildGenresList(barChartContent.genres),
          SizedBox(
            height: 20,
          ),
          _buildTopSongsList(),
        ],
      );
    } else {
      return FutureBuilder<Artist>(
        future: _artistDataLoader.getArtistById(barChartContent.detailID),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              textBaseline: TextBaseline.alphabetic,
              children: [
                _createHorizontalCardSlider(barChartContent.detailTitle,
                    barChartContent.image, snapshot.data.followers.total),
                _buildGenresList(snapshot.data.genres),
                SizedBox(
                  height: 20,
                ),
                _buildTopSongsList(),
              ],
            );
          } else if (snapshot.hasError) {
            log.e(
                'The future builder which loads artists by id ran into an unexpected error');
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              ScaffoldMessenger.of(context).showSnackBar(getFeatureErrorSnackBar());
            });
            return CenteredProgressIndicator();
          }
          log.d('The future builder which loads artists by id is loading data');
          // By default, show a loading spinner.
          return CenteredProgressIndicator();
        },
      );
    }
  }

  /// creates multiple cards that can be slided through depending on [snapshot]
  HorizontalCardSlider _createHorizontalCardSlider(
      String title, DartImage.Image image, int follower) {
    log.d(
        '_createHorizontalCardSlider was called from the build method from the artist details screen');
    return HorizontalCardSlider(
      height: heightCardSlider,
      initialPage: 0,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch, // add this
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Container(height: heightCardSlider * 0.6, child: image),
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              title,
              style: WHITE_TITLE_TEXTSTYLE,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              softWrap: false,
            ),
          ],
        ),
        Column(
          children: [
            Text(
              "Followers",
              style: TextStyle(
                  fontSize: 25,
                  color: SPOTIFY_WHITE,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              NumberFormat.decimalPattern('eu').format(follower),
              style: TextStyle(
                  fontSize: 35,
                  color: SPOTIFY_GREEN,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ],
    );
  }

  /// Builds a line separated list displaying the genres of an artists
  ///
  /// If genres are available they just will be displayed, if not they will be loaded from the given url
  Widget _buildGenresList(List<String> genres) {
    return Container(
        alignment: Alignment(-1, 0),
        child: LineSeparatedList(
          genres,
          100,
          title: "Genres",
        ));
  }

  /// Builds a line separated list displaying the top songs of an artist
  Widget _buildTopSongsList() {
    log.d(
        '_buildTopSongsList was called from the build method from the artist details screen');

    return Container(
      alignment: Alignment(-1, 0),
      child: FutureBuilder<TopTracksFromArtistModel>(
        future: _trackDataLoader.getTopTracksFromArtist(barChartContent.href),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return LineSeparatedList(
              _convertTrackListToTitleList(snapshot.data.tracks),
              screenHeight - 534,
              title: "Top Tracks",
            );
          } else if (snapshot.hasError) {
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              ScaffoldMessenger.of(context).showSnackBar(getFeatureErrorSnackBar());
            });
            return CenteredProgressIndicator();
          }
          // By default, show a loading spinner.
          return CenteredProgressIndicator();
        },
      ),
    );
  }

  /// Reduces a list from tracks to a list of their titles
  List<String> _convertTrackListToTitleList(List<Track> tracks) {
    return tracks.map((t) => t.name).toList();
  }
}
