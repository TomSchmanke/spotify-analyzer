import 'package:flutter/material.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';

import '../../logger.util.dart';

/// Highest wrapper which should be used in every feature screen.
///
/// For implementing the 'go back'-arrow and the title.
class MasterScreen extends StatelessWidget {
  final log = getLogger();

  final String title;
  final Widget child;
  final Color backgroundColor;

  MasterScreen({this.title = '', this.child, this.backgroundColor});

  /// builds 'go back'-arrow and title
  @override
  Widget build(BuildContext context) {
    log.d('The build method of the master screen is called');

    return Scaffold(
        backgroundColor: backgroundColor,
        body: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  child: Icon(
                    Icons.arrow_back_sharp,
                    color: SPOTIFY_WHITE,
                    size: 30,
                  ),
                  onTap: () {
                    log.i('go back arrow was pressed');
                    Navigator.pop(context);
                  },
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    child: Text(
                  title,
                  style: WHITE_TITLE_TEXTSTYLE,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  softWrap: false,
                ))
              ],
            ),
            SizedBox(
              height: 10,
            ),
            child,
          ],
        ));
  }
}
