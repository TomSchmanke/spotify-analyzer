import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/data_handling/artist_data_loader.dart';
import 'package:spotify_analyzer/data_handling/playlist_data_loader.dart';
import 'package:spotify_analyzer/model/image_list_content.dart';
import 'package:spotify_analyzer/model/json_models/basic_models.dart';
import 'package:spotify_analyzer/model/json_models/playlist_followers_model.dart';
import 'package:spotify_analyzer/model/json_models/playlist_song_model.dart';
import 'package:spotify_analyzer/reusable_widgets/centered_progress_indicator.dart';
import 'package:spotify_analyzer/reusable_widgets/custom_radar_chart.dart';
import 'package:spotify_analyzer/reusable_widgets/feature_error_snack_bar.dart';
import 'package:spotify_analyzer/reusable_widgets/horizontal_card_slider.dart';
import 'package:spotify_analyzer/reusable_widgets/line_separated_list.dart';
import 'package:spotify_analyzer/views/reusable_screens/master_screen.dart';

import '../../logger.util.dart';

/// Displays more detailed information for a given playlist.
///
/// Contains its tracks and a genre radar chart.
class PlaylistAnalyzerDetailScreen extends StatefulWidget {
  final PlaylistDetailData content;
  final ConnectionService connectionService;

  const PlaylistAnalyzerDetailScreen(this.content, this.connectionService);

  @override
  _PlaylistAnalyzerDetailScreen createState() =>
      _PlaylistAnalyzerDetailScreen();
}

class _PlaylistAnalyzerDetailScreen
    extends State<PlaylistAnalyzerDetailScreen> {
  final log = getLogger();

  double screenHeight;
  PlaylistDetailData content;
  ConnectionService _connectionService;
  PlaylistDataLoader _playlistDataLoader;
  ArtistDataLoader _artistDataLoader;

  PlaylistContentModel playlistContentModel;
  final int maxRadarChartEntries = 12;
  final double minPercentageToDisplay = 0.1;

  // widget heights
  final double heightUpperPart = 76;
  final double heightCardSlider = 320;

  @override
  void initState() {
    log.d('The init method of the playlist analyzer details screen is called');
    super.initState();
    content = widget.content;
    _connectionService = widget.connectionService;
    _playlistDataLoader = new PlaylistDataLoader(_connectionService);
    _artistDataLoader = new ArtistDataLoader(_connectionService);
  }

  /// Builds a detail screen for playlists.
  ///
  /// Implements a horizontal card slider containing a playlist title image
  /// and radar chart as well as a [LineSeparatedList] for a list of all
  /// playlists' tracks
  @override
  Widget build(BuildContext context) {
    log.i('The build method of the playlist analyzer details screen is called');
    screenHeight = MediaQuery.of(context).size.height;
    return MasterScreen(
      title: 'Playlist Details',
      backgroundColor: SPOTIFY_STANDARD_GREY,
      child: FutureBuilder(
        future: _getAllGenresCounted(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: [
                _createHorizontalCardSlider(snapshot),
                LineSeparatedList(_buildListContent(playlistContentModel),
                    screenHeight - (heightUpperPart + heightCardSlider),
                    title: 'Songs')
              ],
            );
          } else if (snapshot.hasError) {
            log.e(
                'The future builder which loads the genres ran into an unexpected error');
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              ScaffoldMessenger.of(context).showSnackBar(getFeatureErrorSnackBar());
            });
            return CenteredProgressIndicator();
          }
          log.d('The future builder which loads the genres is loading data');
          // By default, show a loading spinner.
          return CenteredProgressIndicator();
        },
      ),
    );
  }

  /// creates multiple cards that can be slided through depending on [snapshot]
  HorizontalCardSlider _createHorizontalCardSlider(
      AsyncSnapshot<dynamic> snapshot) {
    log.d(
        '_createHorizontalCardSlider was called from the build method from the playlist analyzer details screen');
    return HorizontalCardSlider(
      height: heightCardSlider,
      initialPage: 1,
      children: [
        Column(
          children: [
            Text(
              "Followers",
              style: TextStyle(
                  fontSize: 25,
                  color: SPOTIFY_WHITE,
                  fontWeight: FontWeight.bold),
            ),
            FutureBuilder<PlaylistFollowerModel>(
              future: _playlistDataLoader.getPlaylistFollowers(content.id),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  int followers = snapshot.data.followers.total;
                  return Text(
                    NumberFormat.decimalPattern('eu').format(followers),
                    style: TextStyle(
                        fontSize: 35,
                        color: SPOTIFY_GREEN,
                        fontWeight: FontWeight.bold),
                  );
                } else if (snapshot.hasError) {
                  log.e(
                      'The future builder which loads the playlist followers ran into an unexpected error');
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                    ScaffoldMessenger.of(context)
                        .showSnackBar(getFeatureErrorSnackBar());
                  });
                  return CenteredProgressIndicator();
                }
                log.d(
                    'The future builder which loads the playlist followers is loading data');
                // By default, show a loading spinner.
                return CenteredProgressIndicator();
              },
            )
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch, // add this
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Container(
                  height: heightCardSlider * 0.6, child: content.image),
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              content.title,
              style: WHITE_TITLE_TEXTSTYLE,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              softWrap: false,
            ),
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _createRadarChartWidget(
              snapshot,
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              "Genres",
              style: WHITE_TITLE_TEXTSTYLE,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              softWrap: false,
            ),
          ],
        )
      ],
    );
  }

  /// builds list content depending on PlaylistContentModel [contentList]
  List<String> _buildListContent(PlaylistContentModel contentList) {
    log.d(
        '_buildListContent was called from the build method from the playlist analyzer screen screen');
    List<String> childrenList = [];
    contentList.items.forEach((element) {
      childrenList.add(element.track.name);
    });
    return childrenList;
  }

  /// Returns list of all top tracks' ArtistIDs as List<String> by using
  /// jsonLoader.getTopTracks(...)
  Future<List<String>> _getTopTracksArtistsIds() async {
    playlistContentModel = await _playlistDataLoader
        .getPlaylistsTracks(content.detailHref, limit: 50, offset: 0);
    return _getAllArtistIdsFromTracks(playlistContentModel.items);
  }

  /// Returns a List<String> of all artist ids for the given [tracks]
  List<String> _getAllArtistIdsFromTracks(List<PlaylistSong> tracks) {
    // 1: extract the artists, 2: flatten the 2D List, 3: extract the ids
    return tracks
        .map((track) => track.track.artists)
        .expand((artists) => artists)
        .map((artist) => artist.id)
        .toList();
  }

  /// Returns map of all genres and their total occurrences in TopTracks-Artists
  /// as Map<String, int>-Future (uses API Calls)
  /// with key as genre name (String) and value as counted occurrences of that genre
  Future<Map<String, int>> _getAllGenresCounted() async {
    log.d(
        '_getAllGenresCounted was called from the build method from the playlist analyzer details screen');
    return _getTopTracksArtistsIds().then((artistIds) async {
      return _getGenresOfArtists(artistIds).then((genres) {
        Map<String, int> genreCount = new Map();
        genres.forEach((genre) {
          if (!genreCount.containsKey(genre)) {
            genreCount[genre] = 1;
          } else {
            genreCount[genre] += 1;
          }
        });
        return genreCount;
      });
    });
  }

  /// Returns a list of all genres for the given [artistIds] (includes duplicates)
  Future<List<String>> _getGenresOfArtists(List<String> artistIds) async {
    // split all artist ids into chunks of 50
    // so that the spotify api can handle the request below with max 50 ids:
    List<List<String>> splitArtistIdsBy50 = [];
    for (int n = 0; n < artistIds.length; n += 50) {
      splitArtistIdsBy50
          .add(artistIds.sublist(n, min(n + 50, artistIds.length)));
    }

    // collect all artists' genres
    List<String> genres = [];
    for (List<String> artistIds in splitArtistIdsBy50) {
      List<Artist> artists = await _artistDataLoader.getArtistsByIds(artistIds);
      // collect only genres, flatten them into List<String> and add to genres
      genres.addAll(artists.map((artist) => artist.genres).expand((g) => g));
    }
    return genres;
  }

  /// Returns the main radar chart widget for given async snapshot [snapshot]
  ///
  /// Includes error handling and loading animation
  /// Note: Sorting of entries is possible by using [sortingType]
  Widget _createRadarChartWidget(AsyncSnapshot<dynamic> snapshot,
      {SortingType sortingType = SortingType.alphabetical}) {
    if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.hasData) {
        Map<String, dynamic> radarData =
            _calcRadarChartData(snapshot.data, sortingType: sortingType);
        return Container(
          padding: EdgeInsets.only(left: 12, right: 12),
          child: CustomRadarChart(
            radarData['labels'],
            radarData['values'],
            chartRadiusFactor: 0.8,
          ),
        );
      } else if (snapshot.hasError) {
        return Text(
          'An Error has occurred.\n${snapshot.error}\n\nPlease restart the app.',
          style: WHITE_NORMAL_TEXTSTYLE,
        );
      }
    }
    return CenteredProgressIndicator();
  }

  /// Calculates radar chart data as Map with entries
  ///
  /// labels: List<String>, values: List<double>
  Map<String, dynamic> _calcRadarChartData(Map<String, int> genreCount,
      {SortingType sortingType}) {
    // sort topGenres by values and write sorted keys to list (with max n entries)
    List<String> sortedGenreNames = genreCount.keys.toList()
      ..sort((k1, k2) => genreCount[k2].compareTo(genreCount[k1]));
    // find maximum value
    int maxValue =
        sortedGenreNames.length > 0 ? genreCount[sortedGenreNames[0]] : 1;

    // remove all low value-entries until length of maxRadarChartEntries reached.
    // remove all entries with values below minPercentageToDisplay * maxValue
    sortedGenreNames
      ..removeRange(
          min(maxRadarChartEntries, genreCount.length), genreCount.length)
      ..removeWhere(
          (element) => genreCount[element] < minPercentageToDisplay * maxValue);

    // if intended, sort entries differently
    switch (sortingType) {
      case SortingType.value:
        // already sorted by values (see above)
        break;
      case SortingType.alphabetical:
        sortedGenreNames.sort((g1, g2) => g1.compareTo(g2));
        break;
      case SortingType.random:
        sortedGenreNames..shuffle(new Random());
        break;
    }

    // get values for RadarChart from genreCount[genreName]
    List<double> values = sortedGenreNames
        .map((genreName) => 100 * genreCount[genreName] / maxValue)
        .toList();

    return {'labels': sortedGenreNames, 'values': values};
  }
}

enum SortingType { value, alphabetical, random }
