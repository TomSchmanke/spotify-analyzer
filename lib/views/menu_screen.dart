import 'package:flutter/material.dart';
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/views/feature_screens/playlist_analyzer.dart';
import 'package:spotify_analyzer/views/feature_screens/top_artists.dart';
import 'package:spotify_analyzer/views/feature_screens/top_tracks.dart';

import '../logger.util.dart';
import 'feature_screens/audio_features.dart';
import 'feature_screens/top_genres.dart';

/// The main menu, containing all core features as different cards.
class MenuScreen extends StatefulWidget {
  final _connectionService;

  const MenuScreen(this._connectionService);

  @override
  State<StatefulWidget> createState() => _MenuScreen();
}

class _MenuScreen extends State<MenuScreen> {
  final log = getLogger();
  ConnectionService _connectionService;

  @override
  void initState() {
    log.d('The init method of the MenuScreen screen is called');
    _connectionService = widget._connectionService;
    super.initState();
  }

  /// builds a List of cards leading to the different feature screens.
  @override
  Widget build(BuildContext context) {
    log.i('The build method of the MenuScreen screen is called');
    return new Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: SPOTIFY_BACKGROUND_GREY,
        body: ListView(children: <Widget>[
          SizedBox(
            height: 8,
          ),
          createRow(
              'Playlist Analyzer',
              Icons.playlist_play_outlined,
              'Analyses all your songs of a selected playlist.',
              PlaylistAnalyzerScreen(_connectionService)),
          createRow(
              'Top Artists',
              Icons.person_outline_rounded,
              'See statistics for the artists you\'ve heard most.',
              TopArtistsScreen(_connectionService)),
          createRow(
              'Top Tracks',
              Icons.adjust_outlined,
              'List your top tracks of all time.',
              TopTracksScreen(_connectionService)),
          createRow(
              'Audio Features',
              Icons.insert_chart,
              'Show features to your top tracks.',
              AudioFeaturesScreen(_connectionService)),
          createRow(
            'Top Genres',
            Icons.widgets_outlined,
            'Visualize your favourite genres.',
            TopGenresScreen(_connectionService),
          ),
          SizedBox(height: 16),
          Center(
            child: MaterialButton(
                child: Text(
                  'Logout',
                  style: WHITE_SMALL_TEXTSTYLE,
                ),
                color: SPOTIFY_SWITCH_DARK_GREY,
                onPressed: () {
                  log.i('Requesting logout.');
                  Navigator.pop(context);
                }),
          )
        ]));
  }

  /// Creates a custom container leading to a feature screen [widget] on click
  /// Contains [content] as title, [icon] as feature icon and [description] as
  /// feature description
  Container createRow(String content, IconData icon, String description,
      StatefulWidget widget) {
    log.d(
        'createRow was called from the build method from the MenuScreen screen');
    return Container(
        padding: EdgeInsets.all(8),
        child: Container(
          height: 96,
          child: Material(
              borderRadius: BorderRadius.circular(12),
              color: SPOTIFY_STANDARD_GREY,
              child: InkWell(
                onTap: () {
                  log.i('The ' +
                      widget.toStringShort() +
                      ' button was pressed in the menu screen');
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => widget));
                },
                borderRadius: BorderRadius.circular(12),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Icon(
                        icon,
                        color: SPOTIFY_GREEN,
                        size: 40,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(content, style: WHITE_TITLE_TEXTSTYLE),
                        SizedBox(height: 4),
                        Text(description, style: GREY_SMALL_TEXTSTYLE),
                      ],
                    )
                  ],
                ),
              )),
        ));
  }
}
