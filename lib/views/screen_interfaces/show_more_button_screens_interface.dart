abstract class IsShowMoreButtonScreen {
  int getLength();

  void setLimit(int limit);
}
