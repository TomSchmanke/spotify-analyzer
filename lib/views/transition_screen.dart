import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/constants/colors_constants.dart';
import 'package:spotify_analyzer/constants/text_style_constants.dart';
import 'package:spotify_analyzer/reusable_widgets/centered_progress_indicator.dart';

import '../logger.util.dart';
import 'menu_screen.dart';

/// Transition screen for switching between LoginScreen and MenuScreen
/// Includes loading, exit and error handling
class TransitionScreen extends StatefulWidget {
  final ConnectionService _connectionService;

  const TransitionScreen(this._connectionService);

  @override
  _TransitionScreen createState() => _TransitionScreen();
}

class _TransitionScreen extends State<TransitionScreen>
    with WidgetsBindingObserver {
  TransitionState _transitionState;
  String _errorMessage;

  Duration _showTextOnLoadingDuration = Duration(seconds: 10);
  final log = getLogger();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    this.reload();
    log.d('The initState method of the transition screen is called.');
  }

  /// reload the authentication process
  /// sets [_transitionState] and [_errorMessage] on error
  void reload() {
    widget._connectionService
        .authenticate()
        .then((e) => log.i('Web page loaded.'))
        .catchError((err) => handleAuthError(err));

    // set screen into loading state
    this.setState(() {
      _transitionState = TransitionState.loading;
      _errorMessage = '';
    });
  }

  /// remove auth observer
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  /// check for AppLifecycleState.resumed and continue by receiving tokens
  /// tries to load MenuScreen on success, otherwise: error
  /// sets: [_transitionState] and [_errorMessage]
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      widget._connectionService
          .receiveAccessAndRefreshToken()
          .then((success) => {
                success
                    ? openMenuScreen().then((value) => {
                          this.setState(() {
                            log.i('Transition to exit state.');
                            _transitionState = TransitionState.exit;
                          }),
                        })
                    : this.setState(() {
                        log.i('Transition to error state.');
                        _transitionState = TransitionState.error;
                        _errorMessage = 'Login failed. Please try again.';
                      }),
              })
          .catchError((err) => handleAuthError(err));
    }
  }

  void handleAuthError(dynamic err) {
    log.e('Authentication Error. ' + err.toString());
    this.setState(() {
      _transitionState = TransitionState.error;
      _errorMessage =
          'The spotify authentication process failed.\n' + err.toString();
    });
  }

  /// open menu screen via Navigator
  Future<dynamic> openMenuScreen() {
    log.d('Forward to MenuScreen.');
    return Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MenuScreen(widget._connectionService)));
  }

  /// build the transition screen depending on [_transitionState] and [_errorMessage]
  @override
  Widget build(BuildContext context) {
    log.d('The build method of the transition screen is called.');
    return new Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: SPOTIFY_BACKGROUND_GREY,
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 64),
              Expanded(child: getCurrentScreenStatusWidget()),
              createBottomLogoSection(),
            ]));
  }

  /// loads screen widget depending on current [_transitionState]
  Widget getCurrentScreenStatusWidget() {
    switch (this._transitionState) {
      case TransitionState.loading:
        return createLoadingScreen();
      case TransitionState.exit:
        return createExitScreen();
      case TransitionState.error:
        return createErrorScreen();
    }
    return Container(
      child: Text(this._errorMessage),
    );
  }

  /// Loading screen Widget
  Widget createLoadingScreen() {
    log.i('Transitioning to loading screen.');
    return Column(children: [
      CenteredProgressIndicator(),
      FutureBuilder(
          future: Future.delayed(_showTextOnLoadingDuration),
          builder: (c, s) => (s.connectionState == ConnectionState.done &&
                  !widget._connectionService.didWebPageOpen())
              ? Column(children: [
                  SizedBox(height: 16),
                  Text('This takes longer than expected...',
                      style: GREY_NORMAL_TEXTSTYLE),
                ])
              : Container())
    ]);
  }

  /// Exit screen widget
  /// Enables logout confirmation and rescinding on the other hand
  Widget createExitScreen() {
    log.i('Transitioning to exit screen.');
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text('Logout', style: GREEN_TITLE_TEXTSTYLE),
        SizedBox(height: 8),
        Text('Do you want to logout?', style: WHITE_HEADLINE_TEXTSTYLE),
        SizedBox(height: 16),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          MaterialButton(
              child: Text(
                'No, go back',
                style: WHITE_SMALL_TEXTSTYLE,
              ),
              color: SPOTIFY_SWITCH_STANDARD_GREY,
              onPressed: () {
                log.i('Logout aborted.');
                openMenuScreen();
              }),
          SizedBox(width: 40),
          MaterialButton(
              child: Text(
                'Yes',
                style: WHITE_SMALL_TEXTSTYLE,
              ),
              color: SPOTIFY_SWITCH_STANDARD_GREEN,
              onPressed: () {
                Navigator.pop(context);
                // make sure to really logout and delete the user's tokens
                widget._connectionService.clearTokens();
                log.i('Logout confirmed.');
              }),
        ]),
      ],
    );
  }

  /// Error screen Widget
  /// Enables error handling by going back to home page or reload the request
  /// Includes an individual error message: [_errorMessage]
  Widget createErrorScreen() {
    log.i('Transitioning to error screen.');
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Align(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('Something went wrong', style: GREEN_TITLE_TEXTSTYLE),
                  SizedBox(height: 8),
                  Text(
                    this._errorMessage,
                    style: GREY_SMALL_TEXTSTYLE,
                  ),
                  SizedBox(height: 32),
                  Text(
                    'Try to reload the authentication process, '
                        'go back to the home page or restart the app.\n\n',
                    style: WHITE_NORMAL_TEXTSTYLE,
                  ),
                ]),
            alignment: Alignment.centerLeft,
          ),
        ),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          MaterialButton(
              child: Text(
                'Back to home page',
                style: WHITE_NORMAL_TEXTSTYLE,
              ),
              color: SPOTIFY_SWITCH_STANDARD_GREY,
              onPressed: () {
                Navigator.pop(context);
                widget._connectionService.clearTokens();
                log.i('Going back to home page.');
              }),
          SizedBox(width: 40),
          MaterialButton(
              child: Text(
                'Reload',
                style: WHITE_NORMAL_TEXTSTYLE,
              ),
              color: SPOTIFY_SWITCH_STANDARD_GREEN,
              onPressed: () {
                this.reload();
                // make sure to really logout and delete the user's tokens
                widget._connectionService.clearTokens();
                log.i('Reload authentication process.');
              }),
        ]),
        Container(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Align(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 32),
                  Text(
                    'Besides that, feel free to contact our developers for further help.',
                    style: WHITE_NORMAL_TEXTSTYLE,
                  ),
                ]),
            alignment: Alignment.centerLeft,
          ),
        ),
      ],
    );
  }

  /// Bottom logo section
  Widget createBottomLogoSection() {
    return Container(
        padding: EdgeInsets.only(bottom: 16),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 128,
                child: Image.asset('lib/assets/logo/app_icon_fg.png'),
              ),
            ]));
  }
}

enum TransitionState { loading, exit, error }
