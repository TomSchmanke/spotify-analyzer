import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../auth/connection_service.dart';
import '../constants/colors_constants.dart';
import '../logger.util.dart';
import 'transition_screen.dart';

/// The home page screen, containing a login button.
class LogInScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LogInScreenState();
}

class _LogInScreenState extends State {
  final log = getLogger();
  final ConnectionService _connectionService = new ConnectionService();

  @override
  initState() {
    log.d('The init method of the LogInScreen screen is called');
    _connectionService.lifeCheck();
    super.initState();
  }

  /// builds the home page as a login screen with simple text in spotify style,
  /// login button and a register link
  @override
  Widget build(BuildContext context) {
    log.i('The build method of the LogInScreen screen is called');
    return new Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: SPOTIFY_BACKGROUND_GREY,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(15.0, 90.0, 0.0, 0.0),
                    child: Text('Spotify',
                        style: TextStyle(
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold,
                            color: SPOTIFY_WHITE)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(16.0, 155.0, 0.0, 0.0),
                    child: Text('Analyzer',
                        style: TextStyle(
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold,
                            color: SPOTIFY_WHITE)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(325.0, 155.0, 0.0, 0.0),
                    child: Text('.',
                        style: TextStyle(
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold,
                            color: SPOTIFY_GREEN)),
                  )
                ],
              ),
            ),
            SizedBox(height: 140.0),
            Container(
              padding: EdgeInsets.only(left: 90.0, right: 90.0),
              height: 40.0,
              child: Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: Colors.greenAccent,
                  color: SPOTIFY_GREEN,
                  elevation: 7.0,
                  child: InkWell(
                    onTap: () {
                      log.i('Log In button was pressed');
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  TransitionScreen(_connectionService)));
                    },
                    borderRadius: BorderRadius.circular(20.0),
                    child: Center(
                      child: Text(
                        'LOGIN WITH SPOTIFY',
                        style: TextStyle(
                            color: SPOTIFY_WHITE, fontWeight: FontWeight.bold),
                      ),
                    ),
                  )),
            ),
            Expanded(
              child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'New to Spotify?',
                            style: TextStyle(color: SPOTIFY_WHITE),
                          ),
                          SizedBox(width: 5.0),
                          InkWell(
                            onTap: () {
                              log.i('Register link was pressed');
                              _launchURL('https://www.spotify.com/de/signup/');
                            },
                            child: Text(
                              'Register',
                              style: TextStyle(
                                  color: SPOTIFY_GREEN,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline),
                            ),
                          ),
                        ])),
              ),
            ),
          ],
        ));
  }

  /// launches an [url], used by register button.
  _launchURL(String url) async {
    if (await canLaunchUrl(Uri.parse(url))) {
      await canLaunchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }
}
