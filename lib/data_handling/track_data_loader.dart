import 'dart:convert';

import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/data_handling/data_loader.dart';
import 'package:spotify_analyzer/model/json_models/top_tracks_from_artist_model.dart';
import 'package:spotify_analyzer/model/json_models/top_tracks_model.dart';

import '../logger.util.dart';

class TrackDataLoader extends DataLoader {
  TrackDataLoader(ConnectionService _connectionService)
      : super(_connectionService);

  // most error logs are handled in DataLoader
  final log = getLogger();

  /// Fetches the Top Tracks of a user. Use [limit] to limit the amount of
  /// results. Use [timeRange] to set the time span of the results.
  Future<TopTracksModel> getTopTracks(
      {String timeRange = 'medium_term', int limit = 25}) async {
    String url =
        'https://api.spotify.com/v1/me/top/tracks?time_range=$timeRange&limit=$limit';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      TopTracksModel data = TopTracksModel.fromJson(jsonDecode(response.body));
      log.i("Successfully loaded and converted data from " + url);
      return data;
    } else {
      throw Exception('Failed to load the top tracks from the Spotify Web API');
    }
  }

  /// Fetches only the TracksIds of the users Top Tracks.
  Future<List<String>> getTopTracksIds(
      {String timeRange = 'medium_term', int limit = 25}) async {
    String url =
        'https://api.spotify.com/v1/me/top/tracks?time_range=$timeRange&limit=$limit';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      TopTracksModel topTracksModel =
          TopTracksModel.fromJson(jsonDecode(response.body));
      List<String> data =
          topTracksModel.items.map((track) => track.id).toList();
      log.i("Successfully loaded and converted data from " + url);
      return data;
    } else {
      throw Exception('Failed to load the top tracks from the Spotify Web API');
    }
  }

  /// Fetches the Top Tracks of an Artist.
  Future<TopTracksFromArtistModel> getTopTracksFromArtist(String url) async {
    url = url + '/top-tracks?country=DE';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      TopTracksFromArtistModel data = TopTracksFromArtistModel.fromJson(
          jsonDecode(response.body));
      log.i("Successfully loaded and converted data from " + url);
      return data;
    } else {
      throw Exception('Failed to load the resources from the Spotify Web API');
    }
  }
}
