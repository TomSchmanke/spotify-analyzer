import 'dart:convert';

import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/data_handling/data_loader.dart';
import 'package:spotify_analyzer/model/json_models/playlist_followers_model.dart';
import 'package:spotify_analyzer/model/json_models/playlist_model.dart';
import 'package:spotify_analyzer/model/json_models/playlist_song_model.dart';

import '../logger.util.dart';

class PlaylistDataLoader extends DataLoader {
  PlaylistDataLoader(ConnectionService connectionService)
      : super(connectionService);

  // most error logs are handled in DataLoader
  final log = getLogger();

  /// Gets a users personal playlists.
  Future<UserPlaylistsModel> getPersonalPlaylists(
      {int limit = 50, int offset = 0}) async {
    String url =
        'https://api.spotify.com/v1/me/playlists?limit=$limit&offset=$offset';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      UserPlaylistsModel data =
          UserPlaylistsModel.fromJson(jsonDecode(response.body));
      log.i("Successfully loaded and converted data from " + url);
      return data;
    } else {
      throw Exception(
          'Failed to load the users playlists from the Spotify Web API');
    }
  }

  /// Fetches the Tracks of a Playlist.
  Future<PlaylistContentModel> getPlaylistsTracks(String url,
      {int limit = 50, int offset = 0}) async {
    url = url + '?limit=$limit&offset=$offset';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      PlaylistContentModel data = PlaylistContentModel.fromJson(
          jsonDecode(response.body));
      log.i("Successfully loaded and converted data from " + url);
      return data;
    } else {
      throw Exception(
          'Failed to load the playlists content from the Spotify Web API');
    }
  }

  /// Fetches more data of the playlist to a given [playlistId].
  Future<PlaylistFollowerModel> getPlaylistFollowers(String playlistId) async {
    String url = 'https://api.spotify.com/v1/playlists/$playlistId';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      PlaylistFollowerModel data = PlaylistFollowerModel.fromJson(
          jsonDecode(response.body));
      log.i("Successfully loaded and converted data from " + url);
      return data;
    } else {
      throw Exception(
          'Failed to load the playlists content from the Spotify Web API');
    }
  }
}
