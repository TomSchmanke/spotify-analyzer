import 'dart:convert';

import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/data_handling/data_loader.dart';
import 'package:spotify_analyzer/model/json_models/audio_feature_model.dart';

import '../logger.util.dart';

class AudioFeaturesDataLoader extends DataLoader {
  AudioFeaturesDataLoader(ConnectionService _connectionService)
      : super(_connectionService);

  // most error logs are handled in DataLoader
  final log = getLogger();

  /// Fetches Audio Features to multiply Tracks.
  Future<List<AudioFeaturesModel>> getAudioFeatures(
      List<String> trackIds) async {
    String ids = trackIds.join(',');
    String url = 'https://api.spotify.com/v1/audio-features?ids=$ids';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['audio_features'];
      List<AudioFeaturesModel> convertedData = data
          .map((audioFeatureJson) =>
          AudioFeaturesModel.fromJson(audioFeatureJson))
          .toList();
      log.i("Successfully loaded and converted data from " + url);
      return convertedData;
    } else {
      throw Exception(
          'Failed to load the audio features to the top tracks from the Spotify Web API');
    }
  }
}