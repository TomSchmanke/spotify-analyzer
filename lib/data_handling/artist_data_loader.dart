import 'dart:convert';

import 'package:spotify_analyzer/auth/connection_service.dart';
import 'package:spotify_analyzer/data_handling/data_loader.dart';
import 'package:spotify_analyzer/model/json_models/basic_models.dart';
import 'package:spotify_analyzer/model/json_models/top_artists_model.dart';

import '../logger.util.dart';

class ArtistDataLoader extends DataLoader {
  ArtistDataLoader(ConnectionService connectionService)
      : super(connectionService);

  // most error logs are handled in DataLoader
  final log = getLogger();

  /// Fetches the Top Artists of a user. Use [limit] to limit the amount of
  /// results. Use [timeRange] to set the time span of the results.
  Future<TopArtistsModel> getTopArtists(
      {String timeRange = 'medium_term', int limit = 25}) async {
    String url =
        'https://api.spotify.com/v1/me/top/artists?time_range=$timeRange&limit=$limit';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      TopArtistsModel data =
          TopArtistsModel.fromJson(jsonDecode(response.body));
      log.i("Successfully loaded and converted data from " + url);
      return data;
    } else {
      throw Exception('Failed to load the resources from the Spotify Web API');
    }
  }

  /// Fetches Artist data from the Spotify Web API to an [artistId].
  Future<Artist> getArtistById(String artistId) async {
    String url = 'https://api.spotify.com/v1/artists/$artistId';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      Artist artist = Artist.fromJson(jsonDecode(response.body));
      log.i("Successfully loaded and converted data from " + url);
      return artist;
    } else {
      throw Exception(
          'Failed to load the artist with id $artistId from the Spotify Web API');
    }
  }

  /// Fetches several Artist data from the Spotify Web API based on a
  /// List of [artistIds].
  Future<List<Artist>> getArtistsByIds(List<String> artistIds) async {
    String idsSeparated = artistIds.join(",");
    String url = 'https://api.spotify.com/v1/artists?ids=$idsSeparated';

    final response = await fetchData(url);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['artists'];
      List<Artist> artists =
      data.map((artist) => Artist.fromJson(artist)).toList();
      log.i("Successfully loaded and converted data from " + url);
      return artists;
    } else {
      throw Exception(
          'Failed to load the artists with ids $idsSeparated from the Spotify Web API');
    }
  }
}
