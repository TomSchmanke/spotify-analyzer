import 'package:http/http.dart' as http;
import 'package:spotify_analyzer/auth/connection_service.dart';

import '../logger.util.dart';

abstract class DataLoader {
  ConnectionService _connectionService;

  DataLoader(this._connectionService);

  final log = getLogger();

  // ---------------------------------------------------------------------------
  // --------------------------- fetch data ------------------------------------
  // ---------------------------------------------------------------------------

  /// Used to fetch data from the given url. If the access-token is
  /// expired there will be a refresh of the access-token und the
  /// request to the Spotify Web API will be retried.
  Future<http.Response> fetchData(String url) async {
    Map<String, String> headers =
        _getHeaders(_connectionService.getAccessToken());

    log.d('Fetching data from Spotify API: ' + url);
    final response = await http.get(Uri.parse(url), headers: headers);

    if (response.statusCode == 401) {
      log.d('Expired tokens. Trying to refresh tokens.');
      _connectionService.refreshTokens().then((success) {
        return success
            ? fetchData(url)
            : {
                log.e('Failed refreshing tokens...'),
                throw Exception('Could not refresh your access token.')
              };
      });
    } else if (response.statusCode != 200) {
      log.e(
          'Failed fetching data from Spotify API. Response code:  ${response.statusCode}');
      throw Exception(
          'Failed to load data from the Spotify Web API. Response code: ${response.statusCode}.');
    }
    log.i('Success fetching data from Spotify API.');
    return response;
  }

  // ---------------------------------------------------------------------------
  // ------------------------------ Header -------------------------------------
  // ---------------------------------------------------------------------------

  Map<String, String> _getHeaders(String accessToken) {
    Map<String, String> headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + accessToken,
    };
    return headers;
  }
}
