# Spotify Analyzer 
Spotify Analyzer is a mobile application developed in dart with flutter. The app was initially developed within the scope of the lecture "Entwicklung mobiler Applikationen" (development of mobile applications) at the DHBW Mannheim.

## Current Version
The current version of Spotify Analyzer is [v1.1](https://gitlab.com/TomSchmanke/spotify-analyzer/-/releases).

## License
Spotify Analyzer is licensed unter the [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/).

## Changelog
#### v1.0
- first release

#### v1.1
- click animation in login and menu screen
- redesign of details screens
    - horizontal card slider with page indicator
    - followers card
- hide the top system bar
- new state names for the term slider
- proper error messages
- loading screen for login
- error screen for login
- new screen for logout
- proper behavior for text overflow
- 'show more'-button now loads all 50 songs/artists
- add tooltip to bar chart
- reduce waiting time for login

## Table of Content
[[_TOC_]]

## Features
Spotify Analyzer makes it possible to analyze your [Spotify](https://www.spotify.com/) user statistics the whole year and not only when the Spotify Wrapped gets published at the end of the year.

### Playlist Analyzer
This screen lists all of your playlist. By clicking on one of your playlists you see the image of the playlist and all songs beneath the image. If you swipe the image to the left you see an alysis of the prominent genres in the playlist. By swiping all the way to the right you can see how many followers the playlist has.

<img src="/lib/assets/screenshots/playlist-analyzer.png" alt="Playlist Analyzer" height=500 width=250 />
<img src="/lib/assets/screenshots/playlist-details1.png" alt="Playlist Details" height=500 width=250 />
<img src="/lib/assets/screenshots/playlist-details2.png" alt="Playlist Details" height=500 width=250 />
<img src="/lib/assets/screenshots/playlist-details3.png" alt="Playlist Details" height=500 width=250 />


### Top Artists
The Top Artists screen shows your most listened artists. At first only your top five songs are being displayed. By clicking the 'show more'-button you can reveal your top 50 artists. You also have the opportunity to vary the time range of the analysis by using a slider. The bar chart which is shown right next to the artists picture shows the popularity of the artist. The popularity is calculated based on the popularity of each song of the artist. If you click at one of the artists you are able to see the genres of the artists' and the top tracks of them. By swiping the artists' image to the left you can see how many followers the artist has.

<img src="/lib/assets/screenshots/top-artists.png" alt="Top Artists" height=500 width=250 />
<img src="/lib/assets/screenshots/artist-details1.png" alt="Artist Details" height=500 width=250 />
<img src="/lib/assets/screenshots/artist-details2.png" alt="Artist Details" height=500 width=250 />


### Top Tracks
The Top Tracks screen looks similar to the Top Artists screen. You see the songs that you listened most to. The bar chart right next to the tracks' picture shows the popularity of the song. The popularity gets calculated by an alogrithm from Spotify. The result is highly based on the times the song got played. You can also adjust the considered time range and with the 'show more'-button you are able to show up to 50 songs. By clicking on a song you can see its artists' genres and top tracks. If you swipe the artists' image to the left you can see how many followers the artis has.

<img src="/lib/assets/screenshots/top-tracks.png" alt="Top Tracks" height=500 width=250 />
<img src="/lib/assets/screenshots/track-details1.png" alt="Artist Details" height=500 width=250 />
<img src="/lib/assets/screenshots/track-details2.png" alt="Artist Details" height=500 width=250 />


### Audio Features
The Audio Features screen shows you an evaluation of your top tracks based on acousticness, danceability, energy, instrumentalness, liveness, speechiness and valence in a radar chart. By clicking the information sign at the upper right corner, the app redirects you to a screen with a more detailed description of the attributes. You are able to vary how many tracks are analyzed and what time range is considered.

<img src="/lib/assets/screenshots/audio-features.png" alt="Audio Features" height=500 width=250 />


### Top Genres
The Top Genres screen gives a visual representation of the genres you listen to the most. You are able to adjust the time range and you can group by keywords. Keywords give you a more basic understanding of the genres.

<img src="/lib/assets/screenshots/top-genres.png" alt="Top Genres" height=500 width=250 />
<img src="/lib/assets/screenshots/top-genres-grouped.png" alt="Top Genres grouped" height=500 width=250 />


## Usage of external data
* **Spotify Web API**: For the users Spotify usage data we are using the [Spotify Web API](https://developer.spotify.com/documentation/web-api/).
* **Java wrapper for Spotify Web API**: For the user login we wrote our own [login service](https://github.com/Laura-Schmidt-ls/spotify-analyzer-service) which uses the [Java wrapper for Spotify Web API](https://github.com/thelinmichael/spotify-web-api-java) from [Michael Thelin](https://github.com/thelinmichael). We could have used the whole functionality but this would have exceeded our expectation for our exam performance of the lecture "Entwicklung mobiler Applikationen".
* **multi_charts**: [multi_charts](https://pub.dev/packages/multi_charts) is a flutter package for diagrams of all kinds. We used it for our radar charts.
* **flutter_swiper** and **smooth_page_indicator**: [flutter_swiper](https://pub.dev/packages/flutter_swiper) is a flutter package for swiping content pages and [smooth_page_indicator](https://pub.dev/packages/smooth_page_indicator) indicates the currently active page.

## Installation Guide
This guide refers to the official Flutter documentation for building and installing a Flutter app under [Android](https://flutter.dev/docs/deployment/android) and [iOS](https://flutter.dev/docs/deployment/ios). To build and install the Spotify Analyzer you need to set up Flutter on your computer. For this we recommend to follow the [official installation guide](https://flutter.dev/docs/get-started/install) for Flutter. After doing this you can follow the steps below.

### Build and Install an APK on your Android device
1. **Dowload** the **latest release** and unpack it in a directory of your choice
2. Open your **cmd**
3. **Navigate** to the **directory** you unpacked the release in (you can do this with `cd <app dir>` by replacing "app dir" with the directory you unpacked the release in)
4. Run `flutter build apk`
5. The APK will be created at `<app dir>/build/app/outputs/apk/release/app-release.apk`
6. **Connect** your Android device to your computer with a **USB cable**
7. Enable **USB Debugging** on your device
    1. Open the device's Settings menu
    2. Scroll down to About phone
    3. Tap on Build number about 7 times until you see a confirmation message
    4. Tap the back button und you will see the Developer options menu appear at the bottm of the settings page
    5. Tab on it and scroll down until you see USB debugging, press it and hit Ok to enable it
8. Run `flutter install`

### Build and Install the app on an iOS device
Due to the lack of resource we weren't able to try the installation process nor the functionality on an iOS device. Therefore we only refer to the official Flutter documentation for installing a Flutter app on an [iOS](https://flutter.dev/docs/deployment/ios) device.
